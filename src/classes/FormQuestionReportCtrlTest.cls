/**
* @name    : FormQuestionReportCtrlTest 
* @content : Unit test for this class
* @created : (2016/05/16) De.NX
* @modified: 
*/
@isTest
private class FormQuestionReportCtrlTest {
    
    @testSetup
    static void setUp() {
         
    }
    
     /**
    * @name     : Test page size label config.
    * @case     : Page size is not a number.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPageSizeConfig_notANumber() {
    }
    
    /**
    * @name     : Test page size label config.
    * @case     : Page size is a number.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPageSizeConfig_isANumber() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        
        PageReference opptyPage = Page.FormQuestionReportPage;
        Test.setCurrentPage(opptyPage);
        ApexPages.currentPage().getParameters().put('id', util.formId);
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        // - When
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());

    }
    
    /**
    * @name     : Test page parameter .
    * @case     : Form id is null.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPageParameter_formIdNull() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        // - When
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());
        
        System.assertEquals(0, ctrl.data.totalRecords);
    }
    
    /**
    * @name     : Test page parameter .
    * @case     : Form id is not null.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPageParameter_formIdNotNull() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        PageReference opptyPage = Page.FormQuestionReportPage;
        Test.setCurrentPage(opptyPage);
        ApexPages.currentPage().getParameters().put('id', util.formId);

        // - When
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());
        
        System.assertNotEquals(0, ctrl.data.totalRecords);
        System.assertNotEquals(0, ctrl.data.datasource.size());

    }
    
    /**
    * @name     : Test paging move next .
    * @case     : Increase paging parameter.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPagingMoveNext() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        PageReference opptyPage = Page.FormQuestionReportPage;
        Test.setCurrentPage(opptyPage);
        ApexPages.currentPage().getParameters().put('id', util.formId);
        
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        ctrl.data.pageSize = 5;
        ctrl.data.fetchLazyData();
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(5, ctrl.data.datasource.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);

        System.assertEquals(1, ctrl.data.pageNumber);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(true, ctrl.data.allowMvNext);
        System.assertEquals(false, ctrl.data.allowMvPrevious);        
                
        System.assertEquals(1, ctrl.data.lnumber);
        System.assertEquals(5, ctrl.data.rnumber);


        
        // - When
        ctrl.data.mvNext();
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(1, ctrl.data.datasource.size());
        
        System.assertEquals(false, ctrl.data.allowMvNext);
        System.assertEquals(true, ctrl.data.allowMvPrevious);
        
        System.assertEquals(2, ctrl.data.pageNumber);
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(6, ctrl.data.lnumber);
        System.assertEquals(6, ctrl.data.rnumber);

    }

    /**
    * @name     : Test paging move to last.
    * @case     : Increase paging parameter.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPagingMoveLast() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        PageReference opptyPage = Page.FormQuestionReportPage;
        Test.setCurrentPage(opptyPage);
        ApexPages.currentPage().getParameters().put('id', util.formId);
        
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        ctrl.data.pageSize = 5;
        ctrl.data.fetchLazyData();
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(5, ctrl.data.datasource.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);

        System.assertEquals(1, ctrl.data.pageNumber);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(true, ctrl.data.allowMvNext);
        System.assertEquals(false, ctrl.data.allowMvPrevious);        
                
        System.assertEquals(1, ctrl.data.lnumber);
        System.assertEquals(5, ctrl.data.rnumber);


        
        // - When
        ctrl.data.mvToLast();
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(1, ctrl.data.datasource.size());
        
        System.assertEquals(false, ctrl.data.allowMvNext);
        System.assertEquals(true, ctrl.data.allowMvPrevious);
        
        System.assertEquals(2, ctrl.data.pageNumber);
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(6, ctrl.data.lnumber);
        System.assertEquals(6, ctrl.data.rnumber);

    }
    
    /**
    * @name     : Test paging move to previous.
    * @case     : Decrease paging parameter.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPagingMovePrevious() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        PageReference opptyPage = Page.FormQuestionReportPage;
        Test.setCurrentPage(opptyPage);
        ApexPages.currentPage().getParameters().put('id', util.formId);
        
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        ctrl.data.pageSize = 5;
        ctrl.data.fetchLazyData();
        ctrl.data.mvNext();
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(1, ctrl.data.datasource.size());
        
        System.assertEquals(false, ctrl.data.allowMvNext);
        System.assertEquals(true, ctrl.data.allowMvPrevious);
        
        System.assertEquals(2, ctrl.data.pageNumber);
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(6, ctrl.data.lnumber);
        System.assertEquals(6, ctrl.data.rnumber);
        
        // - When
        ctrl.data.mvPrevious();
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(5, ctrl.data.datasource.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);

        System.assertEquals(1, ctrl.data.pageNumber);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(true, ctrl.data.allowMvNext);
        System.assertEquals(false, ctrl.data.allowMvPrevious);        
                
        System.assertEquals(1, ctrl.data.lnumber);
        System.assertEquals(5, ctrl.data.rnumber);

    }

    /**
    * @name     : Test paging move to first.
    * @case     : Decrease paging parameter.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPagingMoveFirst() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        PageReference opptyPage = Page.FormQuestionReportPage;
        Test.setCurrentPage(opptyPage);
        ApexPages.currentPage().getParameters().put('id', util.formId);
        
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        ctrl.data.pageSize = 5;
        ctrl.data.fetchLazyData();
        ctrl.data.mvNext();
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(1, ctrl.data.datasource.size());
        
        System.assertEquals(false, ctrl.data.allowMvNext);
        System.assertEquals(true, ctrl.data.allowMvPrevious);
        
        System.assertEquals(2, ctrl.data.pageNumber);
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(6, ctrl.data.lnumber);
        System.assertEquals(6, ctrl.data.rnumber);
        
        // - When
        ctrl.data.mvToFirst();
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(5, ctrl.data.datasource.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);

        System.assertEquals(1, ctrl.data.pageNumber);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(true, ctrl.data.allowMvNext);
        System.assertEquals(false, ctrl.data.allowMvPrevious);        
                
        System.assertEquals(1, ctrl.data.lnumber);
        System.assertEquals(5, ctrl.data.rnumber);

    }
    
    /**
    * @name     : Test paging move to page.
    * @case     : Jump to page.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPagingMovePage() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        util.createTestResponses();
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        PageReference opptyPage = Page.FormQuestionReportPage;
        Test.setCurrentPage(opptyPage);
        ApexPages.currentPage().getParameters().put('id', util.formId);
        
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        ctrl.data.pageSize = 5;
        ctrl.data.fetchLazyData();
        
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(5, ctrl.data.datasource.size());
        
        System.assertEquals(6, ctrl.data.totalRecords);

        System.assertEquals(1, ctrl.data.pageNumber);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(true, ctrl.data.allowMvNext);
        System.assertEquals(false, ctrl.data.allowMvPrevious);        
                
        System.assertEquals(1, ctrl.data.lnumber);
        System.assertEquals(5, ctrl.data.rnumber);
        
               
        // - When
        ctrl.data.mvToPage(2);
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size());
        
         System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(1, ctrl.data.datasource.size());
        
        System.assertEquals(false, ctrl.data.allowMvNext);
        System.assertEquals(true, ctrl.data.allowMvPrevious);
        
        System.assertEquals(2, ctrl.data.pageNumber);
        System.assertEquals(6, ctrl.data.totalRecords);
        System.assertEquals(2, ctrl.data.totalPages);
        
        System.assertEquals(6, ctrl.data.lnumber);
        System.assertEquals(6, ctrl.data.rnumber);

    }
    
    /**
    * @name     : Test SOQL offset limit.
    * @case     : Limit 2000.
    * @created  : (2016/05/16) De.NX
    * @modified :
    */
    @isTest
    static void testPagingOffsetLimit() {
        // - Given
        FormTestingUtil util = new FormTestingUtil();
        
        Form__c form = [SELECT id FROM Form__c WHERE id = :util.formId];
        ApexPages.StandardController sc = new ApexPages.StandardController(form);
        
        PageReference opptyPage = Page.FormQuestionReportPage;
        Test.setCurrentPage(opptyPage);
        ApexPages.currentPage().getParameters().put('id', util.formId);                
               
        // - When
        FormQuestionReportCtrl ctrl = new FormQuestionReportCtrl(sc);
        ctrl.data.offset = 2000;
        ctrl.data.fetchLazyData();
        
        // - Then
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertEquals(1, pageMessages.size());
        ApexPages.Message msg = pageMessages[0];
        System.assertEquals(System.Label.FORM_FORCE_PAGINATOR_OFFSET_LIMIT_ERROR_MSG, msg.getDetail());

    }


}