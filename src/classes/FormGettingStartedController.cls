public with sharing class FormGettingStartedController {

public Form__c testForm {get;set;}
public Boolean testFormAvailable {get;set;}
public List<String>  questionIds {get;set;}

public FormGettingStartedController()
{
// make sure getting started form doesn't already exist
    questionIds = new List<String>();
    try
    {
        testForm = [select Id, Name From Form__c where Name=:System.Label.FORM_FORCE_SAMPLE_DATA_NAME LIMIT 1];
        testFormAvailable = true;
    }
    catch (Exception e)
    {
        testFormAvailable = false;
    }
    
}

public void makeTestForm()
{
    
    testForm = new Form__c();
    testForm.Name = System.Label.FORM_FORCE_SAMPLE_DATA_NAME;
    testForm.Submit_Response__c = 'empty'; 
    testForm.Form_Container_CSS__c = '#form_container{ margin: 0 auto; width: 600px; box-shadow: 0 0 14px #CCCCCC; -moz-box-shadow: 0 0 14px #CCCCCC; -webkit-box-shadow: 0 0 14px #CCCCCC; }';
    testForm.URL__c = 'http://sbx3-tci01.cs31.force.com/Form';
    insert testForm;
    
    questionIds.add(createQuestion(0));
    questionIds.add(createQuestion(1));
    questionIds.add(createQuestion(2));
    questionIds.add(createQuestion(3));
    
    createResponses();

    testFormAvailable = true;
    
}

public PageReference viewForm()
{
    return new PageReference('/Apex/FormManagerPage?id=' + testForm.Id);
}

public PageReference takeForm()
{
    return new PageReference('/Apex/MainForm?id=' + testForm.Id + '&cId=none&caId=none');
}

public PageReference viewResults()
{
    FormReportFinderUtil rfu = new FormReportFinderUtil();
    String reportId = rfu.findReportId('Form with Questions and Responses');  
    
    String formId = testForm.Id;
    formId = formId.substring(0,15);
    
    return new PageReference('/' + reportId + '?pv0=' + formId);
}

  private String createQuestion(Integer i){
    Form_Question__c q = new Form_Question__c();
    q.Name = 'Testing Question';
    q.Form__c = testForm.Id;
    q.Type__c = getType(i);
    q.Choices__c = getChoices(i);
    q.Question__c = 'Testing Question question' + i;
    q.OrderNumber__c = i;
    q.Required__c = true;
    insert q;
    return q.id;        
  }
  
  private String getType(Integer i){
    if      (i==1)
     return System.Label.FORM_FORCE_QUESTION_TYPE_MULTIPLE_SELECT_VERTICAL;
    else if (i==2)
     return System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL;
    else if (i==3)
     return System.Label.FORM_FORCE_QUESTION_TYPE_FREE_TEXT;
    else
     return System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_HORIZONTAL;
  }
  private String getChoices(Integer i){
    if (i == 0)
        return 'one\ntwo\nthree\n';
    if (i == 1)
        return 'four\nfive\nsix\n';
    if (i == 2)
        return 'seven\neight\nnine\n';

        
    return '';
  }  
  
  private void createResponses()
  {
    Contact c = new Contact();
    try{
        c = [Select Id From Contact where Email=:'formForceAppUser@form.force'];
    }
    catch (Exception e)
    {
        c.LastName = 'Doe';
        c.FirstName = 'Jane';
        c.Email = 'formForceAppUser@form.force';
        c.Password__c = 'trans2016';
        c.mainOwner__c = 'test common user';
    insert c;       
    }
    
    FormTaker__c st = new FormTaker__c();
    st.Contact__c = c.Id;
    st.Form__c = testForm.Id;
    st.Taken__c = 'false';
    insert st;
    
    for (Integer i = 0; i < 4; i ++)
    {
        FormQuestionResponse__c r = new FormQuestionResponse__c();
        if (i == 0) {
            r.Response__c = 'two';
        } else if (i == 1) {
            r.Response__c = 'four';
        } else if (i == 2) {
            r.Response__c = 'nine';
        } else {
            r.Response__c = 'This is a response.';
        }
        Form_Question__c sq = [Select id from Form_Question__c where id=: questionIds[i] limit 1];
        r.Form_Question__c = sq.id;
        r.FormTaker__c = st.Id;
        insert r;   
    }

  }
}