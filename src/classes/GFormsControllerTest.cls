@isTest
public class GFormsControllerTest
{
    private static TestMethod void testMakeNewForm(){
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('uId', Userinfo.getUserId());
        Apexpages.Standardcontroller stc;
        GFormsController sc = new GFormsController(stc);
        System.assert(sc.makeNewForm()==null);
        sc.formSite = '--SELECT SITE--';
        sc.newFormName = 'TestMakeNewForm';
        system.assert(sc.makeNewForm()==null);
        sc.formSite = 'blah';
        System.assert(sc.makeNewForm()!=null);
        sc.doFormReset();     
   }
   
   private static TestMethod void testDeleteForm(){
    FormTestingUtil tu = new FormTestingUtil();
    Apexpages.currentPage().getParameters().put('uId', Userinfo.getUserId());
    Apexpages.Standardcontroller stc;
    GFormsController sc = new GFormsController(stc);
    System.assertEquals(GFormsController.deleteForm(tu.formId),'true');
  }
  
  private static TestMethod void testGFormsController(){
    FormTestingUtil tu = new FormTestingUtil();
    Apexpages.currentPage().getParameters().put('uId', Userinfo.getUserId());
    Apexpages.Standardcontroller stc;
    GFormsController sc = new GFormsController(stc);
    System.assert(sc.allForms.size()>0);
  } 
}