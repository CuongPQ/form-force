public with sharing class viewFormResultsComponentController {

    public String formId {get;set;}
    
    
    public String reportId {get;set;}
    public PageReference results;
    
    public viewFormResultsComponentController()
    {
        FormReportFinderUtil rfu = new FormReportFinderUtil();
        reportId = rfu.findReportId('Form with Questions and Responses'); 
    
    }
    
    
    public pageReference getResults()
    {
        formId = formId.substring(0,15);
        return new PageReference('/' + reportId + '?pv0=' + formId);
    }
}