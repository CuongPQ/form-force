/**
  * SObjectLookupController 
  * @Author: CuongPQ.
  * @Date: 04 Nov 2015.
  * @Description: Detect  information need for call main method open lookup.
  */
public with sharing class SObjectLookupController {
    
    public static string PREFIX_FIELDS_SET= 'TCI_';
    
    public String objName {get;set;}
    public SObject sobj {get;set;} // new account to create
    public List<SObject > results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    public static ApexClass  DEFAULT_NAME_SPACE {
        get{
         List<ApexClass> val = [SELECT NameSpacePrefix FROM ApexClass WHERE Name = 'SObjectLookupController' limit 1];
         return val.size() == 0 ? new ApexClass() : val[0];
        }set;}
    public boolean isAdmin {get;set;}
    
    public SObjectLookupController () {
        try
        {
            this.isAdmin = [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.PermissionsModifyAllData = true AND AssigneeId=:UserInfo.getUserId()].size() == 0 ? false: true;
            String keyPreFix = System.currentPageReference().getParameters().get('key_prefix');
            Id objId = keyPreFix + '000000000000AAA';
            objName = objId.getSObjectType().getDescribe().getName();
            sobj = (SObject)Type.forName(objName).newInstance();
            // get the current search string
            searchString = System.currentPageReference().getParameters().get('lksrch');
            runSearch();
        }
        catch (Exception e)
        {
            system.debug('error when load: ' + e.getMessage());
        }  
    }
    
    public boolean getIsExistedFieldsetNew()
    {
        return getIsExistedFieldset(getDefaultFieldSetForNew());
    }
    
    public boolean getIsExistedFieldsetSearchCondition()
    {
        return getIsExistedFieldset(getDefaultFieldSetForSearchCondition());
    }
    
    public boolean getIsExistedFieldsetSearchResults()
    {
        return getIsExistedFieldset(getDefaultFieldSetForSearchResults());
    }
    
    public boolean getIsExistedFieldset(string pVal)
    {
        try
        {
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(this.objName);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            return DescribeSObjectResultObj.FieldSets.getMap().containsKey(pVal);
        }
        catch (Exception e)
        {
        }
        return false;
    }
    
    public List<String> getListFieldSet()
    {
        List<String> listResult = new List<String>();
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(this.objName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        for (String item : DescribeSObjectResultObj.FieldSets.getMap().keySet())
        {
            if (item == getDefaultFieldSetForNew() || item == getDefaultFieldSetForSearchCondition() ||
                item == getDefaultFieldSetForSearchResults())
            {
                listResult.add(item);
            }
        }
        
        return listResult;
    }
    
    public string getDefaultFieldSetForNew()
    {
        return (DEFAULT_NAME_SPACE.NamespacePrefix == null ?  PREFIX_FIELDS_SET + objName + '_For_New':
            DEFAULT_NAME_SPACE.NamespacePrefix + '__' + PREFIX_FIELDS_SET + objName + '_For_New');
    }
    
    public string getDefaultFieldSetForSearchCondition()
    {
        return (DEFAULT_NAME_SPACE.NamespacePrefix == null ?  PREFIX_FIELDS_SET + objName + '_For_Search_Condition':
            DEFAULT_NAME_SPACE.NamespacePrefix + '__' + PREFIX_FIELDS_SET + objName + '_For_Search_Condition');
    }
    
    public string getDefaultFieldSetForSearchResults()
    {
        return (DEFAULT_NAME_SPACE.NamespacePrefix == null ?  PREFIX_FIELDS_SET + objName + '_For_Search_Results':
            DEFAULT_NAME_SPACE.NamespacePrefix + '__' + PREFIX_FIELDS_SET + objName + '_For_Search_Results');
    }

    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
    
    // prepare the query and issue the search command
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);               
    } 
    
    // run the search and return the records found. 
    private List<SObject> performSearch(string searchString) {
    
        String soql = 'select id, name from ' + this.objName ;
        if (this.objName.toLowerCase() == 'case')
        {
            soql = 'select id, Subject, CaseNumber from ' + this.objName;
        }
        if(searchString != '' && searchString != null)
        {
            if (this.objName.toLowerCase() == 'case')
            {
                soql = soql +  ' where Subject LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\' OR CaseNumber LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\'';
            }
            else
            {
                soql = soql +  ' where name LIKE \'%' + String.escapeSingleQuotes(searchString) +'%\'';
            }
        }
        soql = soql + ' limit 25';
        System.debug(soql);
        return database.query(soql); 
    
    }
    
    // save the new account record
    public PageReference saveOjbRecord() {
        insert sobj;
        // reset the account
        sobj = (SObject)Type.forName(objName).newInstance();
        return null;
    }
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
    // used by the visualforce page to detect show tab create new record of object objName.
    public boolean getPermissionCreate()
    {
        try
        {
            return [SELECT Id, SObjectType, PermissionsCreate
                FROM ObjectPermissions
                WHERE SObjectType =:objName AND PermissionsCreate=true AND parentid in (select id from permissionset where
                PermissionSet.Profile.Id =: UserInfo.getProfileId())].size() > 0;
        }
        catch (Exception e)
        {
            return false;
        }
       
    }

}