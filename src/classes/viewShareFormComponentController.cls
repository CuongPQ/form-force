public with sharing class viewShareFormComponentController {

public Id formId {get;
    set{
        this.formId = value;
        init();
    }

}

public Form__c myForm {get;set;}
public List<SelectOption> urlType {get;set;}
public String selectedURLType {get;set;}
public String POD                                {get; set;}
public List<Selectoption>  sitesPicklist         {get; set;} 
public String formSite                         {get; set;}
public String showAddSitesMessage                {get; set;} 
private String subdomain; 
private boolean useTopLevelDomain;                        
public String siteInfo  {get; set;}

public String formURL{
    get{
        if (selectedURLType == 'Email Link, Anonymous' || selectedURLType == 'Chatter')
        {
            return 'id=' + formId + '&cId=none&caId=none';
        }
        else if (selectedURLType == 'Email Link w/ Contact Merge')
        {
            return 'id=' + formId + '&cId={!Contact.Id}&caId=none';
        }
        else
        {
            return 'id=' + formId +  '&cId={!Contact.Id}&caId={!Case.id}';
        }
    }
    
    set;
}

public String formURLBase{
    get{
            if(formSite == '--SELECT SITE--'){
               POD='';
              return null;
            }
       
            String urlPrefix = setupUrlPrefix(formSite);
            String domain = setupDomainForForm(POD);
            String urlToSave= domain+'/'+urlPrefix+'PreForm?';
            if (formSite == 'Internal' || selectedURLType == 'Chatter')
            {
                if (false == myForm.isPreformPageAcive__c)
                {
                    urlToSave = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/' + 'MainForm?';
                }
                else
                {
                    urlToSave = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/' + 'PreForm?';
                }
            }
            return urlToSave;
    }
    set;
}




public viewShareFormComponentController()
{
    urlType = new List<SelectOption>();
    urlType.add(new SelectOption('Email Link w/ Contact Merge',System.Label.FORM_FORCE_Email_Link_w_Contact_Merge));
    urlType.add(new SelectOption('Email Link w/ Contact & Case Merge',System.Label.FORM_FORCE_Email_Link_w_Contact_Case_Merge));
    urlType.add(new SelectOption('Email Link, Anonymous',System.Label.FORM_FORCE_Email_Link_Anonymous));
    urlType.add(new SelectOption('Chatter',System.Label.FORM_FORCE_Chatter)); 
    selectedURLType = 'Chatter';
    
    setupPOD();
    setupSitesPicklist();
    siteInfo = Site.getDomain();
    
    init();
}

private void setupPOD()
{
    String urlToSplit = URL.getSalesforceBaseUrl().toExternalForm();
    List<String> splitURL = urlToSplit.split('\\.');
    Integer loc = -1;
    Integer i;
    
    
    for (i = 0; i < splitURL.size(); i++)
    {
        
         if(splitURL.get(i) == 'visual' || splitURL.get(i)  == 'salesforce'){
              loc = i - 1;
                 break;
          }
    }
    
    
    if (loc == -1)
    {
        pod = 'NO_POD';
        return;
    }
    
    if(splitURL.get(loc + 1)=='visual')
    {
        pod = splitURL.get(loc);
    }
    else
    {
        pod = 'NO_POD';
    }
    
    
    
}


 private void setupSitesPicklist(){
    
    List<FormSitesUtil.FormSiteInfo> sites = new FormSitesUtil().getSiteList();
    if (sites.size() > 0) {
        subdomain = sites[0].Subdomain;
    } 
    useTopLevelDomain=false;
    String pathPrefix;
    sitesPicklist = new List<Selectoption>();
    sitesPicklist.add(new Selectoption('Internal',System.Label.FORM_FORCE_Internal));
    for(FormSitesUtil.FormSiteInfo current : sites){
      if(current.prefix == null)
        pathPrefix='EMPTY';
      else
        pathPrefix=current.prefix;
        
      sitesPicklist.add(new Selectoption(pathPrefix, current.Name));
    }
    
    formSite = 'Internal';
  }

private String setupDomainForForm(String pod){
    if(pod != 'NO_POD' && !useTopLevelDomain && checkSubdomain(subdomain)){
      return 'http://'+subdomain+'.'+pod+'.force.com';
    }
    else if(pod != 'NO_POD' && useTopLevelDomain && checkSubdomain(subdomain)){
      return 'http://'+subdomain+'.'+pod;
    }
    else if(useTopLevelDomain) {
      return 'http://'+subdomain;   
    }
    else{
      return 'http://'+subdomain+'.force.com';
    }
  }
  
  public boolean checkSubdomain(String subdomain){
    if(subdomain == null)
     return false;
    else if (subdomain.contains('developer-edition'))
     return true;
    else
     return false;
  }
  
  public String setupUrlPrefix(String site){
    if(site == null || site=='EMPTY')
     return '';
    else
     return site+'/';
  }




public void init()
{
    if (formId != null)
    {
        myForm = [Select Id, Name, Form_Header__c,  isPreformPageAcive__c,  isFormConfirmPageAcive__c, URL__c FROM Form__c where Id=:formId];
    }
}
}