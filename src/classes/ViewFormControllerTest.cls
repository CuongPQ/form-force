@isTest
public class ViewFormControllerTest {
     
    private static Testmethod void testViewFormController() {
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);  
        Apexpages.Standardcontroller stc;   
        ViewFormController vsc = new ViewFormController(stc);
        vsc.init();
        System.assert(vsc.allQuestionsSize == 9);
        System.assert(tu.formId != null);
        
        
        vsc.submitResults();
        for (ViewFormController.question q : vsc.allQuestions)
        {
            q.selectedOption = String.valueof('2');
            q.choices = String.valueof('2');
            q.selectedOptions = new List<String>();
            q.selectedOptions.add(String.valueof('2'));
            if (!vsc.validateForm())
            {
                vsc.submitResults();
            }                        
        }
        System.assertEquals(true, vsc.formTakerId != null);
        
        
        //test something
    }
   
   private static Testmethod void testUpdateFormName() {
      FormTestingUtil tu = new FormTestingUtil();
      Apexpages.currentPage().getParameters().put('id',tu.formId);  
      Apexpages.Standardcontroller stc; 
      ViewFormController vsc = new ViewFormController(stc);
      vsc.formName = 'new name';
      system.assert(vsc.updateFormName() == null);
      
  }
  
  private static Testmethod void testExecuteFullForm() {
    FormTestingUtil tu = new FormTestingUtil();
    Apexpages.currentPage().getParameters().put('id',tu.formId);  
    Apexpages.Standardcontroller stc; 
    ViewFormController vsc = new ViewFormController(stc);
    
    system.assert(vsc.getReviewing() == false);
    
    //go to main form.
    PageReference mainPage = vsc.gotoMainForm();    
    system.assert(mainPage.getUrl().contains('MainForm') == true && mainPage.getUrl().contains('id=' + tu.formId)  == true);
    vsc.init();
    system.assert(vsc.validateForm() == true);    
    for (ViewFormController.question q : vsc.allQuestions)
    {
         q.selectedOptions = new List<String>{'1','2'};
         q.selectedOption = '1';
         if (q.renderDateField == 'true')
         {
             q.choices = system.today().format();
         }
         else if (q.renderPasswordField == 'true')
         {
             q.choices = 'Abc1234567';
         }
         else if (q.renderEmailField == 'true')
         {
             q.choices = 'me@yahoo.com';
         }
         else if (q.renderTelField == 'true')
         {
             q.telFull[0] = '1234';
             q.telFull[1] = '1234';
             q.telFull[2] = '1234';
         }
         else
         {
             q.choices = 'one';
         }
    }
    
    //go to page confirm form.
    PageReference confirmPage = vsc.gotoFormConfirm();
    system.assert(confirmPage .getUrl().contains('FormConfirm') == true && confirmPage.getUrl().contains('id=' + tu.formId)  == true);
    
    vsc.initFormConfirm();
    // go to thank form page.
    PageReference thankPage= vsc.gotoThankForm();
    system.assert(thankPage.getUrl().contains('ThankForm') == true && thankPage.getUrl().contains('id=' + tu.formId)  == true);
    
    vsc.initThankForm();
    system.assert(vsc.formTakerName != null);
    
  }
  
  private static Testmethod void testExecuteWithoutConfirmForm() {
    FormTestingUtil tu = new FormTestingUtil();
    Apexpages.currentPage().getParameters().put('id',tu.formIdTemplate);  
    Apexpages.Standardcontroller stc; 
    ViewFormController vsc = new ViewFormController(stc);
    
    //go to main form.
    PageReference mainPage = vsc.gotoMainForm();    
    system.assert(mainPage.getUrl().contains('MainForm') == true && mainPage.getUrl().contains('id=' + tu.formIdTemplate)  == true);
    vsc.init();
    system.assert(vsc.validateForm() == true);    
    for (ViewFormController.question q : vsc.allQuestions)
    {
         q.selectedOptions = new List<String>{'1','2'};
         q.selectedOption = '1';
         if (q.renderDateField == 'true')
         {
             q.choices = system.today().format();
         }
         else if (q.renderPasswordField == 'true')
         {
             q.choices = 'Abc1234567';
         }
         else if (q.renderEmailField == 'true')
         {
             q.choices = 'me@yahoo.com';
         }
         else if (q.renderTelField == 'true')
         {
             q.telFull[0] = '1234';
             q.telFull[1] = '1234';
             q.telFull[2] = '1234';
         }
         else
         {
             q.choices = 'one';
         }
    }
    // go to thank form page.
    PageReference thankPage= vsc.gotoFormConfirm();
    system.assert(thankPage.getUrl().contains('ThankForm') == true && thankPage.getUrl().contains('id=' + tu.formIdTemplate)  == true);
    
  }
  
   private static Testmethod void testExecuteResetForm() {
    FormTestingUtil tu = new FormTestingUtil();
    Apexpages.currentPage().getParameters().put('id',tu.formIdTemplate);  
    Apexpages.Standardcontroller stc; 
    ViewFormController vsc = new ViewFormController(stc);
    vsc.init();
    for (ViewFormController.question q : vsc.allQuestions)
    {
         q.selectedOptions = new List<String>{'1','2'};
         q.selectedOption = '1';
         if (q.renderDateField == 'true')
         {
             q.choices = system.today().format();
         }
         else if (q.renderPasswordField == 'true')
         {
             q.choices = 'Abc1234567';
         }
         else if (q.renderEmailField == 'true')
         {
             q.choices = 'me@yahoo.com';
         }
         else if (q.renderTelField == 'true')
         {
             q.telFull[0] = '1234';
             q.telFull[1] = '1234';
             q.telFull[2] = '1234';
         }
         else
         {
             q.choices = 'one';
         }
    }
    vsc.resetForm();
    system.assert(vsc.allQuestionsConfirm == null && vsc.allQuestions[0].choices == '');
   }
}