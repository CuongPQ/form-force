public with sharing class FormManagerController {

    //@CuongPQ has defined variable constants.-- START 28 Apr 2016 --.
    public static final String PREVIEW_PARAMETER = 'pageName';
    
    public static final String PREVIEW_PREFORM_PAGE = 'PreFormPage';
    
    public static final String PREVIEW_CONFIRM_PAGE = 'ConfirmPage';
    
    public static final String PREVIEW_THANK_PAGE = 'ThankPage';
    
    public static final String PREVIEW_FORM = 'FormPage';
    
    private ApexPages.StandardController stdControl;
    
    //@CuongPQ has added variable make check record type of form that is template.-- START 19 Apr 2016 --.
    public Boolean isTemplate {get;set;}
    
    //@CuongPQ has added flag to check permission of user not admin.
    //public Boolean accessDeny {get;set;}
    
    //@CuongPQ has added flag to check preview tab.--START-- 28 Apr 2016.
    public Boolean isPreviewPreform {get;set;}
    
    public Boolean isPreviewConfirm {get;set;}
    
    public Boolean isPreviewThank {get;set;}
    
    //@CuongPQ has added flag to check preview tab.--END-- 28 Apr 2016.
    
    //@CuongPQ has added flag to check design form tab.--START-- 28 Apr 2016.
    public Boolean isPreviewForm {get;set;}
    
    public Boolean editCSS;
    
    public Boolean getEditCSS(){
        return editCSS;
    } 
    
    public FormManagerController (ApexPages.StandardController stdController){
        this.stdControl = stdController;
        try
        {
            Form__c recordForm = (Form__c)stdController.getRecord();
            system.debug('recordForm :' + recordForm );        
            
            //@CuongpQ had check current form is template form or not. --START 19 Apr 2016.
            this.isTemplate = [SELECT Id FROM RecordType WHERE Name =:System.Label.FORM_FORCE_FORM_RECORD_TYPE_TEMPLATE AND Id=:recordForm.RecordTypeId].size() > 0;
            system.debug('isTemplate :' + isTemplate );
            //--END--.
        }
        catch (Exception e)
        {
            system.debug('[FormManagerController]-contructor : ' + e.getMessage());
        }
        List<User> res = [SELECT Profile.PermissionsAuthorApex FROM User WHERE id=:Userinfo.getUserId()];
        User u = res[0];
        if(u.Profile.PermissionsAuthorApex){
            this.editCSS = true;
        }else{
            this.editCSS = false;
        }
    }
    
    /**
    * --checkAccessDeny--
    * @Author: CuongPQ.
    * @Date: 19 Apr 2016.
    * @Description: restrict access by recordtype for current user.
    * @Param: - none.
    * @return: pageReference : 
    *    - can not accesss => redirect to error page.
    *    - can access => no redirect.
    */
    public pageReference checkAccessDeny()
    {
        if (!FormUtils.getModifyAllPermission() && this.isTemplate)
        {
            return new PageReference('/secur/NoAccess.jsp');
        }
        else
        {
            return null;
        }
    }
    
    /**
    * --previewPage--
    * @Author: CuongPQ.
    * @Date: 28 Apr 2016.
    * @Description: preview preForm page.
    * @Param: - none.
    * @return: pageReference : PreForm    
    */
    public pageReference previewPage() {
        resetPreviewFlag();
        PageReference pageRef = new PageReference('/apex/PreViewForm');
        pageRef.setRedirect(false);
        
        String parameter = Apexpages.currentpage().getParameters().get(PREVIEW_PARAMETER);
        if (parameter == PREVIEW_PREFORM_PAGE)
        {
            this.isPreviewPreform  = true;
        }
        else if (parameter == PREVIEW_CONFIRM_PAGE)
        {
            this.isPreviewConfirm  = true;
        }
        else if (parameter == PREVIEW_THANK_PAGE)
        {
            this.isPreviewThank  = true;
        }
        else if (parameter == PREVIEW_FORM )
        {
            this.isPreviewForm = true;
        }
        return pageRef;
    }
    
    /**
    * --resetPreviewFlag--
    * @Author: CuongPQ.
    * @Date: 28 Apr 2016.
    * @Description: reset Preview flag.
    * @Param: - none.
    * @return: none    
    */
    public void resetPreviewFlag()
    {
        this.isPreviewPreform  = false;
        this.isPreviewConfirm  = false;
        this.isPreviewThank  = false;
    }
}