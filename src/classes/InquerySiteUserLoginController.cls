global with sharing class InquerySiteUserLoginController {
    global String username {get; set;}
    global String password {get; set;}
    public String browserName {get;set;}
 
    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        if (String.isBlank(username)  && String.isBlank(password )) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'ログインＩＤとパスワードを入力してください');
            ApexPages.addMessage(msg);
            return  null;
        }
        else if (String.isBlank(username)) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'ログインＩＤを入力してください');
            ApexPages.addMessage(msg);
            return  null;
        }
        else if (String.isBlank(password)){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'パスワードを入力してください');
            ApexPages.addMessage(msg);
            return  null;
        }

        System.debug('username: ' + username);
        System.debug('password: ' + password);
        
        string encryptPass = InquerySiteUserCommonInfo.encryptData(password, InquerySiteUserCommonInfo.PASSWORD_CRYPTO_KEY);
        System.debug('encryptPass:' + encryptPass);
        
        List<contact> listUser = [SELECT Id, name, email, Password__c, Encrypt_Password__c FROM contact WHERE email=:username AND Encrypt_Password__c=:encryptPass];
        System.debug('listUser common:' + listUser);
        if (listUser.size() > 0)
        {
            System.debug('browserName:' + browserName);
            Cookie sessionCookie  = new Cookie('UserSessionId', InquerySiteUserCommonInfo.generateSessionToken(listUser[0].Id, listUser[0].Encrypt_Password__c, Site.getBaseUrl(),browserName), '/', -1, false);
            ApexPages.currentPage().setCookies(new Cookie[]{sessionCookie});
            if (startURL == null || startURL.length() == 0)
            {
                PageReference page = new PageReference (Site.getBaseUrl());
                page.setRedirect(true);
                return page;
            }
            else
            {
                PageReference orderPage = new PageReference(startURL);
                orderPage.setRedirect(true);
                return orderPage;
            }
            
        }
        else
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'IDまたはパスワードが誤っています');
            ApexPages.addMessage(msg);
        }
        return null;
    }
    
    global InquerySiteUserLoginController () {}
    
    public PageReference checkValidateSession()
    {
        Cookie sessionCookie = ApexPages.currentPage().getCookies().get('UserSessionId');

        if (sessionCookie == null) {
            return null;
        } else {
            PageReference page = System.Page.InquerySiteTemplate;
            page.setRedirect(true);
            return page;
        }
    }
}