public with sharing class FormReportFinderUtil {

  public FormReportFinderUtil(){}
  
  public String findReportId(String reportName){
    
    List<Sobject> myReport = [select Id,Name From Report Where Name=:reportName OR DeveloperName =:reportName];
    system.debug('[FormReportFinderUtil]-findReportId :' + myReport);
      return myReport.size() > 0 ? myReport[0].Id : null;
    
  }
}