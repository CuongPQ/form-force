@isTest
public class viewShareAndResultFormComponentCtrTest  {
    
    private static testmethod void testShareForm()
    {
        Form__c myForm = new Form__c();
        myForm.Submit_Response__c = 'empty';
        myForm.Url__c = 'empty';
        myForm.isPreformPageAcive__c = true;
        insert myForm;
        
        viewShareFormComponentController vss = new viewShareFormComponentController();
        vss.formId = myForm.Id;
        
        vss.selectedURLType = 'Chatter';
        System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm() + '/apex/' + 'PreForm?', vss.formURLBase);
        System.assertEquals('id=' + myForm.Id + '&cId=none&caId=none', vss.formURL);
        
        vss.selectedURLType = 'Email Link w/ Contact Merge';
        System.assertEquals('id=' + myForm.Id + '&cId={!Contact.Id}&caId=none', vss.formURL);
        
        vss.selectedURLType = 'Email Link w/ Contact & Case Merge';
        System.assertEquals('id=' + myForm.Id +  '&cId={!Contact.Id}&caId={!Case.id}', vss.formURL);
    
        System.assertEquals(true, vss.checkSubdomain('test.developer-edition.salesforce'));
        System.assertEquals(false, vss.checkSubdomain(null));
        System.assertEquals(false, vss.checkSubdomain('test.salesforce.com'));
        
        System.assertEquals('', vss.setupUrlPrefix('EMPTY'));
        
        vss.formSite = '--SELECT SITE--';
        System.assertEquals(null, vss.formURLBase); 
    }
    
    private static testmethod void testResultController()
    {
        Form__c myForm = new Form__c();
        myForm.Submit_Response__c = 'empty';
        myForm.Url__c = 'empty';  
        insert myForm;
        
        viewFormResultsComponentController vsr = new viewFormResultsComponentController();
        vsr.formId = myForm.Id;
        
        String myFormId = myForm.Id;
        PageReference pageRef = new PageReference ('/' + vsr.reportId + '?pv0=' + myFormId.substring(0,15));
        System.assertEquals(pageRef.getURL(),vsr.getResults().getURL());
        
    }

}