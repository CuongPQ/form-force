public with sharing class FormSitesUtil {

    

    public Boolean hasSites() {
        return Schema.getGlobalDescribe().keySet().contains('site');
    }
    
    public List<FormSiteInfo> getSiteList() {
        
        List<FormSiteInfo> ret = new List<FormSiteInfo>();
        
        if (!hasSites()) {
            return ret;
        }
        
        List<Sobject> sitesResults = Database.query('Select Name, Subdomain, UrlPathPrefix from Site Where Status = \'Active\'');
                
        for (Sobject current : sitesResults) {
            ret.add(new FormSiteInfo((String)current.get('Name'), (String)current.get('UrlPathPrefix'), (String)current.get('Subdomain')));
        }
        
        return ret;
        
    }
    
    public class FormSiteInfo {
        public String name { get; set; }
        public String prefix { get; set; }
        public String subdomain {get; set; }
        
        public FormSiteInfo(String name, String prefix, String subdomain) {
            this.name = name;
            this.prefix = prefix;
            this.subdomain = subdomain;
        }
    }
    
    static testmethod void testFormSiteInfo() {
        FormSitesUtil.FormSiteInfo info = new FormSitesUtil.FormSiteInfo('one', 'two', 'three');
        System.assertEquals(info.name, 'one');
        System.assertEquals(info.prefix, 'two');
        System.assertEquals(info.subdomain, 'three');
    }
    
    static testmethod void testHasSites() {
        FormSitesUtil util = new FormSitesUtil();
        
        if (Schema.getGlobalDescribe().keySet().contains('site')) {
            System.assert(util.hasSites());
        } else {
            System.assert(!util.hasSites());
        }
    }
    
    static testmethod void testGetInfo() {
        FormSitesUtil util = new FormSitesUtil();
        System.assert(util.getSiteList() != null);
        if (util.hasSites()) {
            if (util.getSiteList().size() > 0) {
                FormSitesUtil.FormSiteInfo current = util.getSiteList()[0];
                System.assert(current != null);
            }
        }
    }
    
}