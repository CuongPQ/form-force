/**
* --FormUtils--
* @Author: CuongPQ
* @Created Date: 15 Apr 2016.
* @Description: Make can access API > 20 for Form App Package.
*/
public with sharing class FormUtils
{
    public static final String RECORD_TYPE_PARAMETER = 'RecordType';
    /**
    * --getModifyAllPermission--
    * @Author: CuongPQ.
    * @Date: 15 Apr 2016.
    * @Description: check permisson of modify all by current user.
    * @return: (boolean) true if current user have permission modify all.
    */
    public static boolean getModifyAllPermission()
    {
        return ([SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.PermissionsModifyAllData = true AND AssigneeId=:UserInfo.getUserId()].size() == 0 ? false: true);
    }
    
    /**
    * --getAvailableRecordType--
    * @Author: CuongPQ.
    * @Date: 15 Apr 2016.
    * @Description: get list record type name.
    * @Param: - pObjName: object api name.
    * @return: List<String> list record type name of object.
    */
    public static List<String> getAvailableRecordType(String pObjName)
    {
        List<String> recordTypeNameList = new List<String>();
        /** Need to create list to collect record type name.**/
        Schema.DescribeSObjectResult R = Schema.getGlobalDescribe().get(pObjName).getDescribe();
        /** You need to change the object name according to your requirement.**/
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        for( Schema.RecordTypeInfo recordType : RT )
        {
            if(recordType.isAvailable())
             { 
                recordTypeNameList.add(recordType.Name);
             }
        }
        return recordTypeNameList;
    }
    
    /**
    * --validateEmail--
    * @Author: CuongPQ.
    * @Date: 27 Apr 2016.
    * @Description: check email data.
    * @Param: - pEmail: email data.
    * @return: Boolean -  the result of checking.
    */
    public static Boolean validateEmail(String pEmail)
    {
        if (Pattern.matches('[0-9a-zA-Z#-+./=?^-`{-~!\\-]+@[0-9a-zA-Z_\\-]+(\\.[0-9a-zA-Z_\\-]+){1,}', pEmail))
        {
            return true;
        }

        return false;
    }
    
    /**
    * --validateTelBlockNumber--
    * @Author: CuongPQ.
    * @Date: 27 Apr 2016.
    * @Description: check tel data.
    * @Param: - pTel: tel data.
    * @return: Boolean -  the result of checking.
    */
    public static Boolean validateTelBlockNumber(String pTel)
    {
        if (String.isBlank(pTel))
        {
            return true;
        }
        boolean length = pTel.length() ==4;
        boolean only1Byte = pTel.length() ==  Blob.valueOf(pTel).size();
        boolean isNumber =  Pattern.matches('[0-9]{4}', pTel);
        
        if (length && only1Byte && isNumber)
        {
            system.debug('[FormUtils].validateTelBlockNumber: check tel number okay');
            return true;
        }
        
        return false;
    }
    
    /**
    * --validateSecret--
    * @Author: CuongPQ.
    * @Date: 27 Apr 2016.
    * @Description: check secret data.
    *    - contain at least 1 number.
    *    - contain at least 1 character.
    *    - length >=8.
    * @Param: - pSecret: secret data.
    * @return: Boolean -  the result of checking.
    */
    public static Boolean validateSecret(String pSecret)
    {
        boolean length = pSecret.length() >= 8;
        boolean pattenCheck = Pattern.matches('(?=.*[0-9])(?=.*[a-zA-Z]).{8,}', pSecret);
        boolean only1Byte = pSecret.length() ==  Blob.valueOf(pSecret).size();
        
        if (String.isBlank(pSecret) || (length  && pattenCheck && only1Byte ))
        {
            return true;
        }
        
        return false;
    }
    
    /**
    * --validateDate--
    * @Author: CuongPQ.
    * @Date: 28 Apr 2016.
    * @Description: check date data.
    * @Param: - pDate: the date need validate.
    * @return: Boolean -  the result of checking.
    */
    public static Boolean validateDate(String pDate)
    {
        if (String.isBlank(pDate))
        {
            return true;
        }
        try
        {
            Date.parse(pDate);
            return true;
        }
        catch(Exception e)
        {
            system.debug('[FormUtils].validateDate: ' + e.getMessage());
        }
        
        return false;
    }
}