/**
@author tci Oyama
@date 2016/04/14
@description アンケートサイトHome
*/
public class FormhomeController {
    public String selectedForm{get; set; }
    
    //@CuongPQ had defined variable to stored contact id and case id. --START 04 May 2016 --.
    public Id contactId {get;set;}
    
    public Id caseId {get;set;}
    
    //--END--.
    
    //コンストラクタ
    public FormhomeController(){
        system.debug('[FormhomeController]- contructor init');
    }
    private List<Form__c> getFormList(){
        
        List<Form__c> pFormList = new List<Form__c>();   
        try{
        //@CuongPQ has added condition filtered record type master only for form.--START 25 Apr 2016.
               pFormList = [Select Id, Name
                              From Form__c
                              Where IsDeleted = false
                              And RecordType.Name =: System.Label.FORM_FORCE_FORM_RECORD_TYPE_MASTER
                             ];
        //--END--
        }catch(Exception err){
            system.debug('getFormList Exception:' + err.getMessage());
        }
        return pFormList;
    }
    public List<SelectOption> getFormListOptions(){
        List<SelectOption> formListOptions = new List<SelectOption>();
        for(Form__c form: getFormList()){
            formListOptions.add(new SelectOption(form.id, form.Name));
        }
        return formListOptions;
    }
    
     /**
    * --getPageName--
    * @Author: CuongPQ.
    * @Date: 11 May 2016.
    * @Description: make select to get the page name to open.
    * @return: blank or preform or mainform.
    */
    public String getPageName()
    {
        List<Form__c> pFormList = [Select Id, Name, isPreformPageAcive__c 
                              From Form__c
                              Where Id=:selectedForm
                             ];
        system.debug('[FormhomeController]getPageName->pFormList ' + pFormList );
        String formName = '';
        if (pFormList.size() > 0)
        {
            if ( pFormList[0].isPreformPageAcive__c == true)
            {
                formName = 'Preform';
            }
            else
            {
                formName = 'MainForm';
            }
            
        }
        return formName ;
    }
    
    /*
    public PageReference gotoTakeForm(){
        system.debug('[FormhomeController]gotoTakeForm->come here ');
        List<Form__c> pFormList = [Select Id, Name, isPreformPageAcive__c 
                              From Form__c
                              Where Id=:selectedForm
                             ];
        system.debug('[FormhomeController]gotoTakeForm->pFormList ' + pFormList );
        if (pFormList.size() > 0)
        {
            String formName;
            if ( pFormList[0].isPreformPageAcive__c == true)
            {
                formName = 'Preform';
            }
            else
            {
                formName = 'MainForm';
            }
            if (contactId == null)
            {
                contactId = '000000000000000AAA';
            }
            if (caseId == null)
            {
                caseId = '000000000000000AAA';
            }
            system.debug('[FormhomeController]gotoTakeForm->url' + Url.getSalesforceBaseUrl().toExternalForm() );
            PageReference pageRef = new PageReference(Url.getSalesforceBaseUrl().toExternalForm() + '/' + formName + '?id=' 
                + selectedForm + '&cId=' + (String.valueOf(contactId).indexOf('000000000000000') >=0 ? 'none' :  contactId) + 
                '&caId=' + (String.valueOf(caseId).indexOf('000000000000000') >=0 ? 'none' :  caseId) );
            return pageRef ;
        }
        return null;
    }
    */
    /*
    *@CuongPQ has commented this method because it do not use.
    *@Date: 15 Apr 2016.
    public String getTakeFormURL(){
        return 'https://sbx-tci01.cs6.force.com/Form/MainForm?id=' + selectedForm;
    }
    */
    public Boolean getIsSelectForm(){
        return selectedForm == null ? false : true;
    } 
}