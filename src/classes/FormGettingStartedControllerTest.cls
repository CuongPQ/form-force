@isTest
public class FormGettingStartedControllerTest
{
    private static testMethod void initGetStartController()
    {
        FormGettingStartedController gst = new FormGettingStartedController();
        system.assert(gst.testFormAvailable == false);
        
        //make test for form.
        gst.makeTestForm();
        system.assert(gst.testFormAvailable == true);
        
        //check goto view form.
        PageReference viewFormPage = gst.viewForm();
        system.assert(viewFormPage.getUrl().contains('/FormManagerPage?id=' + gst.testForm.Id) == true);
        
        //check goto take form.
        PageReference takeFormPage = gst.takeForm();
        system.assert(takeFormPage.getUrl().contains('/MainForm') == true);
        
        //check goto report page.
        PageReference reportPage= gst.viewResults();
        system.debug(reportPage.getUrl());
        system.assert(reportPage.getUrl().contains('?pv0=' + String.valueOf(gst.testForm.Id).subString(0,15)) == true);
    } 
}