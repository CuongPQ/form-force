@isTest
public class FormManagerControllerTest
{
    private static testMethod void initFormanagerControler()
    {
        FormTestingUtil tu = new FormTestingUtil();
        ApexPages.currentPage().getParameters().put('id', tu.formIdTemplate);
        Form__c formDataTemplate= [SELECT Id,RecordTypeId FROM Form__c where Id=:tu.formIdTemplate];
        Apexpages.Standardcontroller std;
        FormManagerController fmc = new FormManagerController(std);
        
        system.assert(fmc.isTemplate == null);
        system.assert(fmc.getEditCSS() == true);
        
        //check access for admin user.
        PageReference pageErr = fmc.checkAccessDeny();
        system.assert(pageErr == null);
        
        System.runAs(tu.createUserStandard()) {
            std = new ApexPages.StandardController(formDataTemplate);
            fmc = new FormManagerController(std);
            system.assert(fmc.isTemplate == true);
            
            //check access for admin user.
            pageErr = fmc.checkAccessDeny();
            system.assert(pageErr.getUrl().contains('secur/NoAccess.jsp') == true);
        }
        
    }
    
    private static testMethod void makePreview()
    {
        FormTestingUtil tu = new FormTestingUtil();
        ApexPages.currentPage().getParameters().put('id', tu.formIdTemplate);
        ApexPages.currentPage().getParameters().put(FormManagerController.PREVIEW_PARAMETER, FormManagerController.PREVIEW_PREFORM_PAGE );
        ApexPages.currentPage().getParameters().put('id', tu.formIdTemplate);
        ApexPages.currentPage().getParameters().put('id', tu.formIdTemplate);
        Form__c formDataTemplate= [SELECT Id,RecordTypeId FROM Form__c where Id=:tu.formIdTemplate];
        Apexpages.Standardcontroller std;
        FormManagerController fmc = new FormManagerController(std);
        
        fmc.previewPage();
        system.assert(fmc.isPreviewPreform  == true);
        
        ApexPages.currentPage().getParameters().put(FormManagerController.PREVIEW_PARAMETER, FormManagerController.PREVIEW_CONFIRM_PAGE);
        fmc.previewPage();
        system.assert(fmc.isPreviewConfirm  == true);
        
        ApexPages.currentPage().getParameters().put(FormManagerController.PREVIEW_PARAMETER, FormManagerController.PREVIEW_THANK_PAGE);
        fmc.previewPage();
        system.assert(fmc.isPreviewThank  == true);
        
        ApexPages.currentPage().getParameters().put(FormManagerController.PREVIEW_PARAMETER, FormManagerController.PREVIEW_FORM );
        fmc.previewPage();
        system.assert(fmc.isPreviewForm == true);
        
    }
}