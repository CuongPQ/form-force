public abstract class LazyDataModel {

    // - Default values
    private static final List<Integer> defaultPageSizes = new List<Integer>{5, 10, 20, 50, 100, 200};
    private static final Integer defaultPageNumber = 1;
    private static final Integer defaultPageIndex = 0;
    private static final Integer defaultOffset = 0;
    private static final Boolean defaultAllowMvNext = false;
    private static final Boolean defaultAllowMvPrevious = false;

    // - Properties
    public List<Object> datasource {
        get;
        set;
    }

    public List<SelectOption> pageSizes {get; set;}
    public Integer pageSize {get; set;}
    public Integer pageNumber {
        get {
            return 0 == this.totalPages ? 0 : pageNumber;
        }
        set;}
    public Integer pageIndex {get; set;}
    public Integer totalRecords {get; set;}
    public Integer totalPages {get; set;}
    public Integer offset {get; set;}
    public Integer lnumber {
        get {
            return 0 == this.totalRecords ? 0 : this.offset + 1;
        }
        set;
    }
    public Integer rnumber {
        get {
            return this.offset + this.pageSize > this.totalRecords ? this.totalRecords : this.offset + this.pageSize;
        }
        set;
    }
    
    public Boolean allowMvNext {
        get{
            this.allowMvNext = (this.pageNumber < this.totalPages);
            return this.allowMvNext;
        } 
        set;
    }
    public Boolean allowMvPrevious {
        get{
            this.allowMvPrevious = (defaultPageNumber < this.pageNumber);
            return this.allowMvPrevious;
        } 
        set;
    }

    // - Init block
    {
      // - Init default pageSizes list
      this.pageSizes = new List<SelectOption>();
      for(Integer d : defaultPageSizes) {
        SelectOption so = new SelectOption(String.valueOf(d), String.valueOf(d));
        this.pageSizes.add(so);
      }
      // - Init pageSize
      this.pageSize = defaultPageSizes.get(0);

      // - Init pageNumber
      this.pageNumber = defaultPageNumber;

      // - Init pageIndex
      this.pageIndex = defaultPageIndex;

      // - Init offset
      this.offset = defaultOffset;
      
      // - Init totalPages
      this.totalPages = 0;
      
      // - Init totalRecords
      this.totalRecords = 0;
      
    }

    // - Constructor
    public LazyDataModel() {

    }

    // - Methods
    public void mvNext() {
        this.pageNumber += 1;
        this.pageIndex +=1;
        this.offset += this.pageSize;
        fetchLazyData();
    }

    public void mvPrevious() {
        this.pageNumber -= 1;
        this.pageIndex -=1;
        this.offset -= this.pageSize;
        fetchLazyData();
    }

    public void mvToFirst() {
        this.pageNumber = defaultPageNumber;
        this.pageIndex = defaultPageIndex;
        this.offset = defaultOffset;
        fetchLazyData();
    }

    public void mvToLast() {
        this.pageNumber = this.totalPages;
        this.pageIndex = this.totalPages - 1;
        this.offset = this.pageSize * (this.totalPages - 1);
        fetchLazyData();
    }

    public void mvToPage(Integer page) {
        if (defaultPageNumber <= page && this.totalPages >= page) {
            this.pageNumber = page;
            this.pageIndex = page - 1;
            this.offset = this.pageSize * (this.pageNumber - 1);
            fetchLazyData();
        }
    }
    
    public void fetchLazyData() {
        
        // - Load totalRecords
        this.totalRecords = loadTotalRecords();
        
        if (0 >= this.totalRecords) {
            return;
        }
        // - Init totalPages
        this.totalPages = this.totalRecords / this.pageSize;
        if (0 != Math.mod(this.totalRecords, this.pageSize)) {
            this.totalPages += 1;
        }
        
        // - Move next or not
        this.allowMvNext = (this.pageNumber < this.totalPages);
        
        // - Load datasource
        this.datasource = load(this.offset, this.pageSize);
        
        
    }
    public abstract Integer loadTotalRecords();
    public abstract List<Object> load(Integer offset, Integer pageSize);
}