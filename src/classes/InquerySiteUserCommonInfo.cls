public with sharing class InquerySiteUserCommonInfo {

    public static string SESSION_CRYPTO_KEY = 'KEY_SFDC_Vn_Dev@';
    public static string PASSWORD_CRYPTO_KEY = 'KEY_SFDC_Jp_Dev@';
    public static String VECTOR = 'KEY_SFDC_TwoTeam';
    private static integer TIME_OUT = 90;
    
    public static string encryptData (String value, string pass)
    {
        Blob vectorIV = Blob.valueOf(VECTOR);
        //16 byte string. since characters used are ascii, each char is 1 byte.
        Blob key = Blob.valueOf(pass);
        //encrypted blob
        Blob cipherText = Crypto.encrypt('AES128', key, vectorIV, Blob.Valueof(value));//Crypto.encryptWithManagedIV('AES128', key, Blob.valueOf(value));
        //encrypted string
        return EncodingUtil.base64Encode(cipherText);
        //return cipherText.toString();
    }
    
    public static string dencryptdData (String encryptString, string pass)
    {
        Blob vectorIV = Blob.valueOf(VECTOR);
        //16 byte string. since characters used are ascii, each char is 1 byte.
        Blob key = Blob.valueOf(pass);
        
        Blob encodedEncryptedBlob = EncodingUtil.base64Decode(encryptString);
        //decrypted blob
        Blob decryptedBlob = Crypto.decrypt('AES128', key, vectorIV, encodedEncryptedBlob);//Crypto.decryptWithManagedIV('AES128', key, encodedEncryptedBlob);
        //decrypted string
        return decryptedBlob.toString();
    }
    
    public static string generateSessionToken (string userId, string pass, string siteUrl, string browserName)
    {
        string plaintextToken = userId + ';' + pass + ';' + siteUrl + ';' + browserName + ';' + system.now(); 
        return encryptData(plaintextToken, SESSION_CRYPTO_KEY);
    }
    public static contact getContactUser(id contctID)
    {
        try
        {
            List<contact> listUser = [SELECT Id, name, email,Password__c ,accountId, account.name, mainOwner__c, Encrypt_Password__c FROM contact WHERE Id=:contctID];
            if (listUser.size() >= 0 ){
                return listUser[0];
            }
            else{
                return null;
            }
        }
        catch (Exception e)
        {
            System.debug('Error at InquerySiteUserCommonInfo class - function \'getSessionUser\':' +  e.getMessage());
            return null;
        }
    }
    public static contact getContactUserByToken(String sessionToken)
    {
        String clearText = dencryptdData(sessionToken, SESSION_CRYPTO_KEY);
        string [] arr = clearText.split(';');        
        if (arr.size() == 5)
        {
            return getContactUser(arr[0]);
            
        }
        return null;
    }
    public static boolean checkValidSessionToken(String sessionToken)
    {
        try
        {
            String clearText = dencryptdData(sessionToken, SESSION_CRYPTO_KEY);
            DateTime compareTime = system.now().addMinutes(-TIME_OUT);
            string [] arr = clearText.split(';');
            system.debug('session detail: ' + arr);
            
            if (arr.size() == 5)
            {
                DateTime lastLogin = DateTime.valueofGmt(arr[4]);
                contact contactUser = getContactUser(arr[0]);
                if (contactUser == null)
                {
                    system.debug('inValid Session because of nothing user');
                    return false;
                }
                    
                system.debug('session created date: ' + lastLogin);
                system.debug('compareTime : ' + compareTime);

                system.debug('arr[1] : ' + arr[1]);
                boolean vs = lastLogin >= compareTime;
                system.debug('compare time out: ' + vs);
                
                if (vs  && contactUser.Encrypt_Password__c == arr[1] )
                {
                    system.debug('Valid Session');
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            System.debug('Error at InquerySiteUserCommonInfo class - function \'checkValidSessionToken\':' +  e.getMessage());
        }
        return false;
    }

  public static PageReference checkLoginCommonUser()
  {
        Cookie cUSessionCookie = ApexPages.currentPage().getCookies().get('UserSessionId');
        System.debug('cUSessionCookie: ' + cUSessionCookie);
        if (cUSessionCookie != null) {
            string commonUserSessionId = cUSessionCookie.getValue();
            boolean isCULogin = (commonUserSessionId== null ? false : InquerySiteUserCommonInfo.checkValidSessionToken(commonUserSessionId));
            if (!isCULogin)
            {
                Cookie pId = new Cookie('UserSessionId', '',null,0,false); // Note the 0 to delete the cookie
                ApexPages.currentPage().setCookies(new Cookie[]{pId});
                return gotoCommonUserLoginPage();
            }
            System.debug('isCULogin: ' + isCULogin);
            System.debug('UserSessionId: ' + commonUserSessionId);
            return null;
        } else {
            return gotoCommonUserLoginPage();
        }
  }
  
  public static PageReference gotoCommonUserLoginPage()
  {
      PageReference page = System.Page.InquerySiteUserLogin;
      page.setRedirect(true);
      return page;
  }
    
}