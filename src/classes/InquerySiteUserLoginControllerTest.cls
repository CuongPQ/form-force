@isTest
public class InquerySiteUserLoginControllerTest
{
    @testSetup
    private static void createTestContact() {
        Contact c = new Contact();
        c.LastName = 'Doe';
        c.FirstName = 'John';
        c.Email = 'formAppUser@hotmail.com.vn';
        c.Password__c ='abc123456';
        c.mainOwner__c = 'test common user';
        insert c;  
        
        Contact c2 = new Contact();
        c2.LastName = 'Doe2';
        c2.FirstName = 'John2';
        c2.Email = 'formAppUser2@hotmail.com.vn';
        c2.Password__c ='abc123456';
        c2.mainOwner__c = 'test common user';
        insert c2;   
    }
    
    private static testMethod void initLoginController()
    {
        InquerySiteUserLoginController uctr = new InquerySiteUserLoginController ();
        
        //make call login function with username and password are empty.
        uctr.login();
        
        system.assert(Apexpages.getMessages()[0].getSeverity() == Apexpages.Severity.ERROR);
        system.assert(Apexpages.getMessages()[0].getSummary() == 'ログインＩＤとパスワードを入力してください');
        
        //make call login function with username is not empty and password is empty.
        uctr.username = 'formAppUser@hotmail.com.vn';
        uctr.login();
        system.assert(Apexpages.getMessages()[1].getSummary() == 'パスワードを入力してください');
        
        //make call login function with username is empty and password is not empty.
        uctr.username = '';
        uctr.password = 'adaddasdasd';
        uctr.login();
        system.assert(Apexpages.getMessages()[2].getSummary() == 'ログインＩＤを入力してください');
        
        //make call login function with username is not empty and password is not empty but password wrong.
        uctr.username = 'formAppUser@hotmail.com.vn';
        uctr.password = 'adaddasdasd';
        uctr.login();
        system.assert(Apexpages.getMessages()[3].getSummary() == 'IDまたはパスワードが誤っています');
        
        //make call login function with username is not empty and password is not empty but password correct.
        uctr.username = 'formAppUser@hotmail.com.vn';
        uctr.password = 'abc123456';
        PageReference redirectPage = uctr.login();
        system.debug('redirectPage : ' + redirectPage );
        system.assert(redirectPage != null);
    }
    
    private static testMethod void checkSection()
    {
        InquerySiteUserLoginController uctr = new InquerySiteUserLoginController ();
        PageReference pageSection= uctr.checkValidateSession();
        system.assert(pageSection == null);
        
        //make call login function with username is not empty and password is not empty but password correct.
        uctr.username = 'formAppUser@hotmail.com.vn';
        uctr.password = 'abc123456';
        uctr.login();
        
        pageSection= uctr.checkValidateSession();
        system.assert(pageSection != null);
    }
    
    private static testMethod void initUserCommonInfo()
    {
        InquerySiteUserLoginController uctr = new InquerySiteUserLoginController ();        
        //not login.
        system.assert(InquerySiteUserCommonInfo.checkLoginCommonUser().getUrl().contains('inquerysiteuserlogin') == true);
        
        //get contact by id.
        system.assert(InquerySiteUserCommonInfo.getContactUser('003000000000000AAA') == null);
        
        //make call login function with username is not empty and password is not empty but password correct.
        uctr.username = 'formAppUser@hotmail.com.vn';
        uctr.password = 'abc123456';
        uctr.login();
        
        //get contact usser by session.
         String sessionCookie = ApexPages.currentPage().getCookies().get('UserSessionId').getValue();
         Contact conVisited = InquerySiteUserCommonInfo.getContactUserByToken(sessionCookie);
         system.assert(conVisited != null);
         
         //check valid sessioncookie.
         system.assert(InquerySiteUserCommonInfo.checkValidSessionToken(sessionCookie) == true);
         
         //check session timeout login.
         system.assert(InquerySiteUserCommonInfo.checkLoginCommonUser() == null);
    }
}