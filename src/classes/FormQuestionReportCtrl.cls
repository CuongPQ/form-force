public with sharing class FormQuestionReportCtrl {

    // - Properties
    private String formId {get; set;}
    
    public LazyDataModel data {get; set;}
    
    public List<String> headerQuestion {get; set;}
    
    // - Constructor
    public FormQuestionReportCtrl(ApexPages.StandardController stdController) {
        // - Get form id from current page
        this.formId = Apexpages.currentPage().getParameters().get('id');
        
        // - Initialize data
        this.data = new QuestionReportModel(this);
        this.headerQuestion = new List<String>();
        for(Form_Question__c qst : [SELECT Question__c, OrderNumber__c FROM Form_Question__c WHERE Form__c = :this.formId ORDER BY OrderNumber__c ASC]) {
            this.headerQuestion.add(qst.Question__c);                    
        }
        system.debug('Size of headerQuestion: ' + this.headerQuestion.size());
        try {
            this.data.pageSize = Integer.valueOf(System.Label.FORM_FORCE_PAGINATOR_PAGE_SIZE);
        } catch(Exception e) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.FORM_FORCE_PAGINATOR_PAGE_SIZE_ERROR_MSG);
            ApexPages.addMessage(msg);
            System.debug('System.Label.FORM_FORCE_PAGINATOR_PAGE_SIZE is not a number, error = ' + e.getMessage()); 
        }
        this.data.fetchLazyData();
    }
    
    // - Inner class
    public class QuestionReportModel extends LazyDataModel {
        // - Properties
        private FormQuestionReportCtrl outerCtrl {get; set;}
        // - Constructor
        public QuestionReportModel(FormQuestionReportCtrl outerIns) {
            this.outerCtrl = outerIns;
        }
        // - Override methods
        public override Integer loadTotalRecords() {
        
            if (null == outerCtrl.formId) {
                return 0;
            }
            AggregateResult[] counter= [SELECT COUNT_DISTINCT(r.FormTaker__c) FROM FormQuestionResponse__c r WHERE r.Form_Question__r.Form__c = :outerCtrl.formId];
            Integer totalRecord = Integer.valueOf(counter[0].get('expr0'));
            return totalRecord;
        }
        public override List<Object> load(Integer offset, Integer pageSize) {
        
            
            List<ResponseReportWrapper> resultItems = new List<ResponseReportWrapper>();
            
            
            if(2000 < offset + pageSize) {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.FORM_FORCE_PAGINATOR_OFFSET_LIMIT_ERROR_MSG);
                ApexPages.addMessage(msg);
                System.debug('SOQL Offset limit to 2000, offset = ' + offset); 
                return resultItems;
            }
            
            
            List<Id> takerIds = new List<Id>();
            for(AggregateResult takerId : [SELECT r.FormTaker__c FROM FormQuestionResponse__c r WHERE r.Form_Question__r.Form__c = :outerCtrl.formId GROUP BY FormTaker__c LIMIT :pageSize OFFSET :offset]) {
                takerIds.add((Id)takerId.get('FormTaker__c'));
            }
            system.debug('Size of takerIds: ' + takerIds.size());
            Integer index = offset;
            Map<Id, ResponseReportWrapper> mergeMap = new Map<Id, ResponseReportWrapper>();
            for(FormQuestionResponse__c itm : [SELECT r.Form_Question__r.OrderNumber__c, r.Response__c, r.FormTaker__c, r.FormTaker__r.Name FROM FormQuestionResponse__c r WHERE r.FormTaker__c IN :takerIds ORDER BY r.FormTaker__c DESC, r.Form_Question__r.OrderNumber__c ASC]) {

                ResponseReportWrapper wper = mergeMap.get(itm.FormTaker__c);
                if(null == wper) {
                    wper = new ResponseReportWrapper();
                    wper.idx = index++;
                    wper.takerId = itm.FormTaker__r;
                    wper.questions = new List<FormQuestionResponse__c>();
                    
                    mergeMap.put(itm.FormTaker__c, wper);
                    resultItems.add(wper);
                }                

                wper.questions.add(itm);
                
            }
            system.debug('Size of resultItems: ' + resultItems.size());
            return resultItems;
        }
    }
    
    public class ResponseReportWrapper {
        // - Properties
        public Integer idx {get; set;}
        public FormTaker__c takerId {get; set;}
        public List<FormQuestionResponse__c> questions {get; set;}
        
    }

}