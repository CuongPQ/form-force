public with sharing class FormTestingUtil {
  public String        formId       {get; set;}
  public List<String>  questionIds    {get; set;}
  public String          contactId      {get; set;}
  public String        formTakerId  {get; set;}
  public String        contactId2     {get; set;}
  public String        formIdTemplate      {get;set;} 
  public String        userStandardId {get; set;}  
  
  public FormTestingUtil(){
    questionIds = new List<String>();
    createTestForm();
    createTestQuestions();
    createTestContact();
    createFormTaker();
    createTestResponses();
    
  }
  
  private static TestMethod void testConstructor(){
    FormTestingUtil tu = new FormTestingUtil();
    System.assert(tu.formId != null);
  }
  
  private void createFormTaker(){
    FormTaker__c st = new FormTaker__c();
    st.Contact__c = contactId;
    st.Form__c = formId;
    st.Taken__c = 'false';
    insert st;
    formTakerId = st.Id;
  }
  
  public void createTestForm(){
    List<String>listRecordTypeName = FormUtils.getAvailableRecordType('Form__c');
    List<RecordType> listRecordType = [SELECT Id, Name FROM RecordType where Name in: listRecordTypeName];
    System.debug('createTestForm-listRecordType: ' + listRecordType );
    Form__c s = new Form__c();
    s.Name = 'Testing Form';
    s.Submit_Response__c = 'empty';
    s.URL__c = 'empty';
    if (listRecordType.size() > 1)
    {
        s.RecordTypeId = listRecordType[0].Name == 'マスター' ? listRecordType[0].Id : listRecordType[1].Id;
        s.isFormConfirmPageAcive__c = true;
    }
    insert s;
    formId = s.Id;
      
    Form__c s1 = new Form__c();
    s1.Name = 'Testing Form 1';
    s1.Submit_Response__c = 'empty';
    s1.URL__c = 'empty';
    if (listRecordType.size() > 1)
    {
        s1.RecordTypeId = listRecordType[0].Name != 'マスター' ? listRecordType[0].Id : listRecordType[1].Id;
    }
    insert s1;
    formIdTemplate = s1.Id;
  }
  
  public void createTestQuestions(){
    for (integer i = 1; i < 10; i++)
    {
       questionIds.add(createQuestion(i)); 
    }
  }
  
  private String createQuestion(Integer i){
    Form_Question__c q = new Form_Question__c();
    q.Name = 'Testing Question';
    q.Form__c = formId;
    q.Type__c = getType(i);
    q.Choices__c = getChoices();
    q.Question__c = 'Testing Question question';
    q.OrderNumber__c = i;
    q.Required__c = true;
    
    Form_Question__c q1 = new Form_Question__c();
    q1.Name = 'Testing Question';
    q1.Form__c = formIdTemplate;
    q1.Type__c = getType(i);
    q1.Choices__c = getChoices();
    q1.Question__c = 'Testing Question question';
    q1.OrderNumber__c = i;
    q1.Required__c = true;
    
    list<Form_Question__c> listQ = new List<Form_Question__c>();
    listQ.add(q);
    listQ.add(q1); 
    insert listQ;
    return listQ[0].id;        
  }
  
  
  private void createTestContact() {
    Contact c = new Contact();
    c.LastName = 'Doe';
    c.FirstName = 'John';
    c.Email = 'formAppUser@hotmail.com';
    c.Password__c ='abc123456';
    c.mainOwner__c = 'test common user';
    insert c;
    contactId = c.Id;   
    
    Contact c2 = new Contact();
    c2.LastName = 'Doe2';
    c2.FirstName = 'John2';
    c2.Email = 'formAppUser2@hotmail.com';
    c2.Password__c ='abc123456';
    c2.mainOwner__c = 'test common user';
    insert c2;
    contactId2 = c2.Id;   
  }
  
  public String createTestResponses() {
    FormQuestionResponse__c r = new FormQuestionResponse__c();
    r.Response__c = 'two';
    Form_Question__c sq = [Select id from Form_Question__c where id=: questionIds[1] limit 1];
    r.Form_Question__c = sq.id;
    r.FormTaker__c = formTakerId;
    insert r;
    return 'true';
  }
  
  private String getType(Integer i){
    if      (i==1)
     return System.Label.FORM_FORCE_QUESTION_TYPE_MULTIPLE_SELECT_VERTICAL;
    else if (i==2)
     return System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL;
    else if (i==3)
     return System.Label.FORM_FORCE_QUESTION_TYPE_FREE_TEXT;
    else if (i==4)
     return System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_HORIZONTAL;
    else if (i==5)
     return System.Label.FORM_FORCE_QUESTION_TYPE_DATE_FIELD;
    else if (i==6)
     return System.Label.FORM_FORCE_QUESTION_TYPE_EMAIL_FIELD;
    else if (i==7)
     return System.Label.FORM_FORCE_QUESTION_TYPE_TEXT_FIELD;
    else if (i==8)
     return System.Label.FORM_FORCE_QUESTION_TYPE_TEL_FIELD;
    else
     return System.Label.FORM_FORCE_QUESTION_TYPE_PASSWORD_FIELD;  
  }
  private String getChoices(){
    return 'one\ntwo\nthree\n';
  }
  
  public User createUserStandard()
  {
      // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name=:System.Label.FORM_FORCE_PROFILE_TEST]; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com.vnn.vn', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com.vnn.vn');
        
        insert u;
        
        userStandardId = u.Id;
        
        return u;
  }
  
}