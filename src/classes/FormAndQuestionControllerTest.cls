@isTest
public class FormAndQuestionControllerTest {
	
    //test constructor and calls within it
    private static Testmethod void testCreateQuestionController(){
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);
        Apexpages.Standardcontroller std;
        FormAndQuestionController cqc = new FormAndQuestionController(std);
        cqc.addQuestion();
        cqc.getNewQuestionNum();
        cqc.makeNewQuestionLink();
        System.assert(cqc.formId == tu.formId);
    }
    
    private static Testmethod void testUpdateFormName() {
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);  
        Apexpages.Standardcontroller stc; 
        FormAndQuestionController vsc = new FormAndQuestionController(stc);
        vsc.formName = 'new name';
        system.assert(vsc.updateFormName() == null);
        
    }
    
    //test constructor and calls within it
    private static TestMethod void testEditQuestion(){
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);
        Apexpages.Standardcontroller std;
        FormAndQuestionController cqc = new FormAndQuestionController(std);
        cqc.editQuestion();
        cqc.questionReference = tu.questionIds[0];
        cqc.editQuestion();
        cqc.questionReference = tu.questionIds[1];
        cqc.editQuestion();
        cqc.questionReference = tu.questionIds[2];
        cqc.editQuestion();
        cqc.questionReference = tu.questionIds[3];
        System.assert(cqc.editQuestion()==null);
        
    }
    
    //test the saving of new questions
    private static TestMethod void testsaveAndNewQuestion(){
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);
        Apexpages.Standardcontroller std;
        FormAndQuestionController cqc = new FormAndQuestionController(std);
        //test saving new question
        cqc.qQuestion = 'THIS IS A NEW QUESTION';
        cqc.qChoices = '1\\n2\\n3\\3';
        cqc.qRequired=true;
        cqc.questionType=System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL;
        cqc.saveAndNewQuestion();
        System.assertEquals(10, cqc.getNewQuestionNum());
        //edit existing question
        FormAndQuestionController cqcI = new FormAndQuestionController(std);
        cqcI.questionReference = tu.questionIds[0];
        cqcI.editQuestion();
        cqcI.qQuestion = 'THIS IS A NEW QUESTION THAT IS EXTRA LONG SO THE NAME SHORTENING CALL WILL BE USED, THIS SHOULD BE LONG ENOUGH NOW THIS IS A NEW';
        cqcI.qChoices = '1\\n2\\n3\\3';
        cqcI.qRequired=true;
        cqcI.questionType=System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL;
        cqcI.saveAndNewQuestion();
        System.assertEquals(10, cqcI.getNewQuestionNum());
    }
    
    private static TestMethod void testsavesaveQuestion(){
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);
        Apexpages.Standardcontroller std;
        FormAndQuestionController cqc = new FormAndQuestionController(std);
        //test saving new question
        cqc.qQuestion = 'THIS IS A NEW QUESTION';
        cqc.qChoices = '1\\n2\\n3\\3';
        cqc.qRequired=true;
        cqc.questionType=System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL;
        cqc.controllerSavQuestion();
        System.assertEquals(10, cqc.getNewQuestionNum());
        //edit existing question
        FormAndQuestionController cqcI = new FormAndQuestionController(std);
        cqcI.questionReference = tu.questionIds[0];
        cqcI.editQuestion();
        cqcI.qQuestion = 'THIS IS A NEW QUESTION THAT IS EXTRA LONG SO THE NAME SHORTENING CALL WILL BE USED, THIS SHOULD BE LONG ENOUGH NOW';
        cqcI.qChoices = '1\\n2\\n3\\3';
        cqcI.qRequired=true;
        cqcI.questionType=System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL;
        cqcI.controllerSavQuestion();
        System.assertEquals(10, cqcI.getNewQuestionNum());
    }
    
    //test constructor and calls within it
    private static TestMethod void testPreviewQuestion(){
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);
        Apexpages.Standardcontroller std;
        FormAndQuestionController cqc = new FormAndQuestionController(std);
        
        cqc.questionReference = tu.questionIds[0];
        cqc.editQuestion();
        cqc.previewQuestion();
        
        cqc.questionReference = tu.questionIds[1];
        cqc.editQuestion();
        cqc.previewQuestion();
        
        cqc.questionReference = tu.questionIds[2];
        cqc.editQuestion();
        System.assert(cqc.previewQuestion()==null);
        
        cqc.questionReference = tu.questionIds[3];
        cqc.editQuestion();
        System.assert(cqc.previewQuestion()==null);
        
    }
    
    private static Testmethod void testupdateFormThankYouAndLink() {
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);  
        Apexpages.Standardcontroller stc; 
        FormAndQuestionController vsc = new FormAndQuestionController(stc);
        //vsc.formThankYouText = 'new stuff';
        //vsc.formThankYouURL = 'more new stff';
        //system.assert(vsc.updateFormThankYouAndLink()==null);
    }
    
    private static Testmethod void testRefreshQuestionList() {
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);
        Apexpages.Standardcontroller stc; 
        FormAndQuestionController vsc = new FormAndQuestionController(stc);
        vsc.getAQuestion();
        
        // Retrieve questions for this form
        List<Form_Question__c> sq = new List<Form_Question__c>();
        sq = [Select id, orderNumber__c from Form_Question__c];
        
        system.debug('vsc.allQuestions: ' + vsc.allQuestions);
        system.debug('sq: ' + sq);
        // get question with orderNumber 1
        Form_Question__c first = [Select id, orderNumber__c from Form_Question__c Where orderNumber__c =: 1 and Form__c =: tu.formId];
        System.assert(first.orderNumber__c == 1 );    
        system.debug('first: ' + first);
        
        // Specify the new order
        vsc.newOrderW = vsc.allQuestions[2].id + ',' +
            vsc.allQuestions[1].id + ',' +
            vsc.allQuestions[0].id + ',' +
            vsc.allQuestions[3].id + ',' + 
            vsc.allQuestions[4].id + ',' + 
            vsc.allQuestions[5].id + ',' +
            vsc.allQuestions[6].id + ',' +
            vsc.allQuestions[7].id + ',' +
            vsc.allQuestions[8].id + ',';
        vsc.updateOrderList();
        
        // Verify that the question with order 1 is not the same as the one retrieved previously
        Form_Question__c second = [Select id, orderNumber__c from Form_Question__c Where orderNumber__c =: 1 and Form__c =: tu.formId];
        System.assert(second.id.equals(first.id) == false);
        
        // update the question list, and make sure it has been modified as well
        vsc.refreshQuestionList();
        system.debug('vsc.qsToUpdate: ' + vsc.allQuestions);
        System.assert(vsc.allQuestions[1].id != first.id);
        
    }
    
    //------------------------------------------------------------------------------//    
    private static TestMethod void testDeleteQuestion() {
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);
        Apexpages.Standardcontroller stc; 
        FormAndQuestionController vsc = new FormAndQuestionController(stc);
        
        
        // Get a question to delete
        Form_Question__c sq = [Select id, orderNumber__c from Form_Question__c Where orderNumber__c =: 1 and Form__c =: tu.formId];
        vsc.questionReference = sq.Id;
        vsc.deleteRefresh();
        
        Form_Question__c sq2 = [Select id, orderNumber__c from Form_Question__c Where orderNumber__c =: 1 and Form__c =: tu.formId];
        System.assert(sq.Id != sq2.Id);
        
        
    }
    //------------------------------------------------------------------------------//
    
    //test the saving of new questions
    private static TestMethod void testFormTemplate(){
        FormTestingUtil tu = new FormTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.formId);
        Apexpages.Standardcontroller std;
        FormAndQuestionController cqc = new FormAndQuestionController(std);
        cqc.getAQuestion();
        //get form template list data.
        System.assert(cqc.getFormTemplatePickList().size() > 0);
        
        //migrate question from master form to template form.
        cqc.selectedFormTemp = tu.formIdTemplate;
        cqc.pickFormTempConfirmAction();        
        
        List<Form_Question__c> listTemplateQuestion = [Select id,orderNumber__c  from Form_Question__c Where Form__c =:tu.formIdTemplate];
        system.debug('cqc.getAQuestion()' + cqc.getAQuestion().size());
        system.debug('listTemplateQuestion' + listTemplateQuestion.size());
        system.assert(cqc.getAQuestion().size() == listTemplateQuestion.size());
    }
}