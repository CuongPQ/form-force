global virtual with sharing class FormAndQuestionController{// extends FormAndQuestionController{
  public String  showTextField                   {get; set;}
  public String  showTextFieldPreview           {get; set;}
  public String  showDateField                   {get; set;}
  public String  showDateFieldPreview           {get; set;}
  public String  showPasswordField                   {get; set;}
  public String  showPasswordFieldPreview           {get; set;}
  public String  showEmailField                   {get; set;}
  public String  showEmailFieldPreview           {get; set;}
  public String  showTelField                   {get; set;}
  public String  showTelFieldPreview           {get; set;}
  //@CuongPQ has add variables handle scenarior pick template and clone childs data. -- START 19 Apr 2016 --.
  public String selectedFormTemp {get;set;}
  // -- END --.
  public List<SelectOption> questionTypeOptions {get; set;}
  public String  showBasicValues                {get; set;}
  public String  showSingleSelect               {get; set;}
  public String  showSingleSelectPreview        {get; set;}
  public String  showMultiSelect                {get; set;}
  public String  showMultiSelectPreview         {get; set;}
  public String  showFreeText                   {get; set;}
  public String  showFreeTextPreview            {get; set;}
  public String  showRowQuestion                {get; set;}
  public String  showRowQuestionPreview         {get; set;}
  public String  showSelectQuestionType         {get; set;}
  public List<SelectOption> singleOptions       {get; set;}
  public List<SelectOption> multiOptions        {get; set;}
  public List<SelectOption> rowOptions          {get; set;}
  public String  questionReference              {get; set;}
  public String  reportId                       {get; set;}
  private Boolean saveAndNew;
  private String  formRef;
  private String formOrderNumber;
  private Form_Question__c questionToUpdate;
  /***/
  public String  qQuestion                      {get; set;}
  public Boolean qRequired                      {get; set;}
  public String  qChoices                       {get; set;}
  public String formName                  {get; set;}
  public String formHeader                {get; set;}
  public String formId                    {get; set;} 
  public String renderFormPreview         {get; set;}  
  public String questionName                {get; set;}  
  public String questionType                {get; set;}
  public Boolean questionRequired           {get; set;}
  public List<question> allQuestions        {get; set;}
  public List<String> responses             {get; set;}
  public Integer allQuestionsSize           {get; set;}
  public String  templateURL                {get; set;}
  //public String  formThankYouText         {get; set;}
  //public String  formThankYouURL          {get; set;}
  public String  caseId                     {get; set;}
    public List<String> newOrder {get;set;}
    public String newOrderW {get;set;}
  
   
  public  FormAndQuestionController (ApexPages.StandardController stdController){
     /**/
    // Get url parameters
    formId = Apexpages.currentPage().getParameters().get('id');
    caseId   = Apexpages.currentPage().getParameters().get('caId');
    //system.debug('FormAndQuestionController - contructor: ' + stdController.getRecord());
    newOrder = new List<String>();
    if(caseId ==null || caseId.length()<5){
      caseId = 'none';
    }
    // By default the preview is not showing up
    renderFormPreview = 'false';

    if (formId != null){ 
      // Retrieve all necessary information to be displayed on the page
      allQuestions = new List<question>();
      setFormNameAndThankYou(formId);
    }
    /**/
    formRef = formId;
    setupQuestionTypeOptions();
    resetViewsToFalse(true);
    showSelectQuestionType = 'False';
    FormReportFinderUtil rfu = new FormReportFinderUtil();
    reportId = rfu.findReportId('Form_with_Questions_and_Responses');
    
    //@CuongPQ has add init vaariable for storing form template picked. -- START 19 Apr 2016 --.
    this.selectedFormTemp = '';  
    //END.
   
  }
  
    /**
    * --pickFormTempConfirmAction--
    * @Author: CuongPQ.
    * @Date: 19 Apr 2016.
    * @Description: do action migrate data current form to selected form template.
    * @return: .
    */
    public void pickFormTempConfirmAction()
    {
        system.debug('the template id: ' + this.selectedFormTemp);
        Savepoint sp = Database.setSavePoint();
        try
        {
            if (this.selectedFormTemp instanceOf Id && this.selectedFormTemp != this.formId)
            {
                //make remove all it's form question.
                List<Form_Question__c> listQuestionOfCurrForm = [SELECT Id FROM Form_Question__c WHERE Form__c=:this.formId];
                System.debug('[FormAndQuestionController]-pickFormTempConfirmAction: list question for delete =' + listQuestionOfCurrForm );
                delete listQuestionOfCurrForm ;

                listQuestionOfCurrForm = new List<Form_Question__c>();
                
                List<Form_Question__c> allQuestionsObject = 
                    [Select s.Type__c, s.Id, s.Form__c, s.Required__c, s.Question__c, 
                    s.OrderNumber__c, s.Name, s.Choices__c 
                    From Form_Question__c s 
                    WHERE s.Form__c =: selectedFormTemp ORDER BY s.OrderNumber__c ASC NULLS LAST];
                
                for (Form_Question__c item : allQuestionsObject )
                {
                    Form_Question__c newQuestion = new Form_Question__c();
                    newQuestion.Form__c = this.formId;
                    newQuestion.Name = item.name;
                    newQuestion.Choices__c = item.Choices__c ;
                    newQuestion.Required__c = item.Required__c;
                    newQuestion.Type__c = item.Type__c;
                    newQuestion.OrderNumber__c = item.OrderNumber__c;
                    newQuestion.Question__c = item.Question__c;
                    listQuestionOfCurrForm.add(newQuestion);
                }
                insert listQuestionOfCurrForm;
                System.debug('[FormAndQuestionController]-pickFormTempConfirmAction: list question for insert =' + listQuestionOfCurrForm );
                
                Form__c updateCurrentForm = [SELECT Id, Name, OwnerId, RecordTypeId FROM Form__c WHERE Id=:this.formId LIMIT 1];
                updateCurrentForm.OwnerId = UserInfo.getUserId();
                
                List<RecordType> listRecordType = [SELECT Id FROM RecordType WHERE Name =:System.Label.FORM_FORCE_FORM_RECORD_TYPE_MASTER];
                if (false == FormUtils.getModifyAllPermission() && listRecordType.size() > 0)
                {
                    updateCurrentForm.RecordTypeId = listRecordType[0].Id;
                }
                System.debug('[FormAndQuestionController]-pickFormTempConfirmAction: form for update=' + updateCurrentForm);
                update updateCurrentForm;
            }
        }
        catch( Exception e)
        {
            System.debug('[FormAndQuestionController]-pickFormTempConfirmAction: ' +  e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
            Database.rollback(sp);
        }
    }
  
    /**
    * --buildFormTemplatePickList--
    * @Author: CuongPQ.
    * @Date: 19 Apr 2016.
    * @Description: build select option of form template data.
    * @return: .
    */
    public List<SelectOption> getFormTemplatePickList()
    {
        List<SelectOption> result = new List<SelectOption>();
        SelectOption op = new SelectOption('000','--なし--');
        result.add(op);
        Integer litmitData = convertLabelIntoInteger(System.Label.FORM_FORCE_FORM_LIMIT_RETRIEVE_TEMPLATE);
        if (litmitData > 0)
        {
            list<RecordType> listRTFormTemp = [SELECT Id FROM RecordType WHERE Name =:System.Label.FORM_FORCE_FORM_RECORD_TYPE_TEMPLATE];
            if (listRTFormTemp.size() > 0)
            {
                List<Form__c> listFormTemp = [SELECT Id, Name FROM Form__c WHERE RecordTypeId=:listRTFormTemp[0].Id AND Id !=:this.formId ORDER BY LastModifiedDate DESC LIMIT :litmitData ];
                for (Form__c item : listFormTemp)
                {
                    op = new SelectOption(item.Id ,Item.Name);
                    result.add(op);
                }
            }
        }
            
        if (result.size() > 1)
        {
            return result;
        }
        system.debug('[FormAndQuestionController]-getFormTemplatePickList : form picklist buit = ' + result);
        return new List<SelectOption>();
    }
    
    /**
    *  @ author: CuongPQ.
    *  @ date: 19 Apr 2016.
    *  @ description: convert the data string to number.
    *  @ param1 pLabel: the label set up by administrator.
    */
    private Integer convertLabelIntoInteger(String pLabel)
    {
            Integer data = -1;
            try
            {
                data = Integer.valueOf(pLabel);
            }
            catch (Exception e)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.FORM_FORCE_FORM_LIMIT_RETRIEVE_TEMPLATE_MES_ERROR);
                ApexPages.addMessage(msg);
                System.debug('[FormAndQuestionController] - convertLabelIntoInteger: ' + e.getMessage()); 
            }
            
            return data ;
    } 
    
  //if you are using a developement org you will need to manually add your Pod.
  /* legacy
  public Pagereference updateUrl(){
    Form__c form = [select Name, Id, URL__c from Form__c where id = :formId];  
    Site subSite = [select Subdomain, id from Site limit 1];
    if(form.URL__c.contains(subSite.Subdomain))
      return null;
    else
      return updateDomain(form, subSite);
  }
  */
  /*
  legacy
  private Pagereference updateDomain(Form__c form, Site subSite){
    String formUrl = form.URL__c;
    String subdomainReplace = 'http://'+subSite.Subdomain;
    //formUrl = formUrl.replaceFirst('*.', subdomainReplace);
    formUrl = formUrl.substring(formUrl.indexOf('.'), formUrl.length());
    formUrl = subdomainReplace + formUrl;
    try{
        form.URL__c = formUrl;
        update form;
    }catch(Exception e){
        system.debug(e);
    }
    setFormNameAndThankYou(formId);
    return null;
  }
  */
  
  public Pagereference makeNewQuestionLink(){
    system.debug('[FormAndQuestionController].makeNewQuestionLink: click add new question');
    questionReference = null;
    resetViewsToFalse(true);
    return null;
  }
  
  public Pagereference editQuestion(){
    if (questionReference == null)
     return null;
    setupEditQuestion();
    showSelectQuestionType = 'True';
    return null;
  }
  
  public Pagereference addQuestion(){
    showSelectQuestionType = 'True';
    resetViewsToFalse(true);
    return null;
  }
  
  private void setupEditQuestion(){
    questionToUpdate =     [Select Type__c, Question__c, Id, Choices__c, Required__c, 
                           OrderNumber__c, Form__c, Name
                           From Form_Question__c 
                           where Id = :questionReference];
    questionType = questionToUpdate.Type__c;
    setupQuestionFields();
    setupEditFields(questionToUpdate);
  }

  private void setupEditFields(Form_Question__c q){
    qQuestion = q.Question__c;
    qRequired = q.Required__c;
    qChoices = q.Choices__c;
    formRef = q.Form__c;    
  }

  private void setupQuestionTypeOptions(){
  
    //get picklist values
    Schema.DescribeFieldResult fieldResult = Form_Question__c.Type__c.getDescribe();
    List<Schema.PicklistEntry>  ple = fieldResult.getPicklistValues();
  
    //set/add them to selectOption list
    questionTypeOptions = new List<SelectOption>();
    questionTypeOptions.add(new SelectOption('--SELECT--', System.Label.FORM_FORCE_SELECTTYPE));
    for(Schema.PicklistEntry pe: ple){
      questionTypeOptions.add(new SelectOption(pe.getLabel(), pe.getValue()));
    }
  }
    
  private void resetViewsToFalse(Boolean clearFields){
    system.debug('[FormAndQuestionController].resetViewsToFalse:' + clearFields);
    showSingleSelect =        'False';
    showSingleSelectPreview = 'False';
    showMultiSelect =         'False';
    showMultiSelectPreview=   'False';
    showFreeText =            'False';
    showFreeTextPreview=      'False';
    showRowQuestion=          'False';
    showRowQuestionPreview=   'False';
    //-- @CuongPQ has set question type of text field with flag show and preview is false. -- START 15 Apr 2016.
    showTextField =            'False';
    showTextFieldPreview=      'False';
    //-- END --.
    //-- @CuongPQ has set question type of date field with flag show and preview is false. -- START 26 Apr 2016.
    showDateField =            'False';
    showDateFieldPreview=      'False';
    //-- END --.
    //-- @CuongPQ has set question type of pasword field with flag show and preview is false. -- START 26 Apr 2016.
    showPasswordField =            'False';
    showPasswordFieldPreview=      'False';
    //-- END --.
    //-- @CuongPQ has set question type of email field with flag show and preview is false. -- START 27 Apr 2016.
    showEmailField =            'False';
    showEmailFieldPreview=      'False';
    //-- END --.
    //-- @CuongPQ has set question type of tel field with flag show and preview is false. -- START 27 Apr 2016.
    showTelField =            'False';
    showTelFieldPreview=      'False';
    //-- END --.
    qRequired =                true;
    if(clearFields){
      qChoices =                 '';
      qQuestion =                '';
      }
    }

  public Pagereference setupQuestionFields(){
    system.debug('[FormAndQuestionController]-(setupQuestionFields): got to setup question fields');
    resetViewsToFalse(false);
    if(questionType==System.Label.FORM_FORCE_QUESTION_TYPE_MULTIPLE_SELECT_VERTICAL){
     showMultiSelect='True';
    }
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL){
      showSingleSelect = 'True';
    }
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_FREE_TEXT){
      showFreeText = 'True';
    }
    //-- CuongPQ has add one option statement into here to check text field. -- START 15 Apr 2016 --.
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_TEXT_FIELD){
      showTextField = 'True';
    }
    // --END --.
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_HORIZONTAL){
      showRowQuestion = 'True';
    }
    //-- CuongPQ has add one option statement into here to check date field. -- START 26 Apr 2016 --.
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_DATE_FIELD){
      showDateField = 'True';
    }
    // --END --.
    //-- CuongPQ has add one option statement into here to check password field. -- START 26 Apr 2016 --.
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_PASSWORD_FIELD){
      showPasswordField = 'True';
    }
    // --END --.
    //-- CuongPQ has add one option statement into here to check email field. -- START 27 Apr 2016 --.
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_EMAIL_FIELD){
      showEmailField = 'True';
    }
    // --END --.
    //-- CuongPQ has add one option statement into here to check tel field. -- START 27 Apr 2016 --.
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_TEL_FIELD){
      showTelField = 'True';
    }
    // --END --.
    return null;
  }  
 
  public Pagereference saveAndNewQuestion(){
    saveAndNew = True;
    if(questionReference == null || questionReference.length() <5)
      return saveNewQuestion();
    else 
      return updateQuestion();  
  }
  
  public Pagereference controllerSavQuestion(){
    if(questionReference == null || questionReference.length() <5){
      return saveNewQuestion();
    }
    else{ 
      return updateQuestion();
    }
  }
  
  private Pagereference updateQuestion(){
    //questionToUpdate is setup in an earlier call to editQuestion()
    questionToUpdate.Name = questionToName(qQuestion);
    questionToUpdate.Choices__c = qChoices;
    questionToUpdate.Required__c = qRequired;
    questionToUpdate.Type__c = questionType;
    questionToUpdate.Question__c = qQuestion;
    try{
      update questionToUpdate;
      resetViewsToFalse(true);
      deleteOldResponses(questionToUpdate.id);
      questionReference = null;
    }catch(Exception e){
      System.debug(e);
      Apexpages.addMessages(e);
    }
    
    return saveOrUpdateReturn();
  }
  
  private void deleteOldResponses(String qId){
    List <FormQuestionResponse__c> sResponses = [select id, Form_Question__c from FormQuestionResponse__c where Form_Question__c = :qId];
    if(sResponses != null)
     delete sResponses;
  }
  
  private Pagereference saveOrUpdateReturn(){
    setupQuestionList();
    Pagereference pr = new Pagereference('/apex/FormPage?id='+formRef);
    questionType = '--SELECT--';
    if(saveAndNew != null  && saveAndNew == true){
      saveAndNew = False;
      showSelectQuestionType = 'True';      
      return pr;
    }
    else{  
      showSelectQuestionType = 'False';      
      return pr; 
    }
  }

  public Integer getNewQuestionNum(){
    if(allQuestions == null)
     return 0;
    else{
     return allQuestions.size();
    }    
  }

  private Pagereference saveNewQuestion(){ 
    Form_Question__c newQuestion = new Form_Question__c();
    newQuestion.Form__c = formRef;
    newQuestion.Name = questionToName(qQuestion);
    newQuestion.Choices__c = qChoices;
    newQuestion.Required__c = qRequired;
    newQuestion.Type__c = questionType;
    newQuestion.OrderNumber__c = getNewQuestionNum();
    newQuestion.Question__c = qQuestion;
    resetViewsToFalse(true);
    try{
      insert newQuestion;
    }catch(Exception e){
      System.debug(e);
    }
    return saveOrUpdateReturn();
  }
  
  private String questionToName(String q){
    
    if(q.length()<75)
     return q;
    else
     return q.substring(0, 75)+'...';
  }
  
  public Pagereference previewQuestion(){
    if(questionType  == System.Label.FORM_FORCE_QUESTION_TYPE_MULTIPLE_SELECT_VERTICAL){
      showMultiSelectPreview = 'True';
      multiOptions = stringToSelectOptions(qChoices);
    }
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL){
     showSingleSelectPreview = 'True';
     singleOptions = stringToSelectOptions(qChoices);
    }
    else if(questionType ==System.Label.FORM_FORCE_QUESTION_TYPE_FREE_TEXT){
      showFreeTextPreview = 'True';
    }
    //-- @CuongPQ has add more statement of question type text-field. -- START 15 Apr 2016 --.
    else if(questionType ==System.Label.FORM_FORCE_QUESTION_TYPE_TEXT_FIELD){
      showTextFieldPreview = 'True';
    }
    //-- END -- .
    else if(questionType == System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_HORIZONTAL){
      showRowQuestionPreview = 'True';
      rowOptions = stringToSelectOptions(qChoices);
    }
    //-- @CuongPQ has add more statement of question type date-field. -- START 26 Apr 2016 --.
    else if(questionType ==System.Label.FORM_FORCE_QUESTION_TYPE_DATE_FIELD){
      showDateFieldPreview = 'True';
    }
    //-- @CuongPQ has add more statement of question type password-field. -- START 26 Apr 2016 --.
    else if(questionType ==System.Label.FORM_FORCE_QUESTION_TYPE_PASSWORD_FIELD){
      showPasswordFieldPreview = 'True';
    }
    //-- END -- .
    //-- @CuongPQ has add more statement of question type password-field. -- START 27 Apr 2016 --.
    else if(questionType ==System.Label.FORM_FORCE_QUESTION_TYPE_EMAIL_FIELD){
      showEmailFieldPreview = 'True';
    }
    //-- END -- .
    //-- @CuongPQ has add more statement of question type tel-field. -- START 27 Apr 2016 --.
    else if(questionType ==System.Label.FORM_FORCE_QUESTION_TYPE_TEL_FIELD){
      showTelFieldPreview = 'True';
    }
    //-- END -- .
    return null;
  }
  
  private List<SelectOption> stringToSelectOptions(String str){
    List<String> strList = str.split('\\r|\n');
    List<SelectOption> returnVal = new List<SelectOption>();
    for(String s: strList){
      returnVal.add(new SelectOption(s,s));
    }
    return returnVal;
    
  }
/****/

  /* Called during the setup of the page. 
     Retrieve questions and responses from DB and inserts them in 2 lists. */
  public Integer setupQuestionList(){
    /*allQuestions.clear();
    List<Form_Question__c> allQuestionsObject = 
                    [Select Type__c, Id, Form__c, Required__c, 
                    Question__c, OrderNumber__c, Name, Choices__c
                    From Form_Question__c  
                    WHERE Form__c =: formId
                    order by OrderNumber__c];
    for (Form_Question__c q : allQuestionsObject){
      question theQ = new question(q);
      allQuestions.add(theQ);
    }
    //responses = getResponses();//taken out because it was SOQL heavy//*/
    getAQuestion();
    return allQuestions.size();
  }
  
  
   /** Sets the form's name variable
  *  param: sID The form ID as specified in the DB
  */
  public void setFormNameAndThankYou(String sId){
    Form__c s = [SELECT Name, Id, URL__c, Form_Header__c FROM Form__c WHERE Id =:sId];
    formName = s.Name;
    formHeader = s.Form_Header__c;
    templateURL = s.URL__c+'id='+sId;//+'&cId={!Contact.Id}'+'&caId='+'{!Case.id}';
    //formThankYouText = s.thankYouText__c;
    //formThankYouURL = s.thankYouLink__c;
  }
  
//------------------------------------------------------------------------------//   
  public Pagereference updateFormName(){
    Form__c s = [SELECT Name, Id, URL__c FROM Form__c WHERE Id =:formId];
    s.Name = formName;
    try{
      update s;
    }catch (Exception e){
      Apexpages.addMessages(e);
    }
    return null;
  } 

//------------------------------------------------------------------------------//    
  /*
  public Pagereference updateFormThankYouAndLink(){
    Form__c s = [SELECT Name, Id, URL__c FROM Form__c WHERE Id =:formId];
    s.thankYouText__c = formThankYouText;
    s.thankYouLink__c = formThankYouURL;
    try{
      update s;
    }catch(Exception e){
      Apexpages.addMessages(e);
    }
    return null;
  }
  */
    
   public PageReference updateOrderList()
  {
    if(newOrderW.length() <= 0)
    {
        return null;
    }
    
    newOrderW = newOrderW.substring(0, newOrderW.length() -1);
    system.debug('[FormAndQuestionController]-updateOrderList.newOrderW : ' + newOrderW);
    List<String> idsToUpdate = newOrderW.split(',',-1);
    List<Form_Question__c> qsToUpdate = new List<Form_Question__c>();
    
    Map<Id,Form_Question__c> questionMap = new Map<Id,Form_Question__c>([select Id, OrderNumber__c from Form_Question__c where Id in :idsToUpdate]);
    Form_Question__c sqToUpdate;
    
    for (Integer i = 0; i < idsToUpdate.size(); i++)
    {
        sqToUpdate = questionMap.get(idsToUpdate.get(i));
        sqToUpdate.OrderNumber__c = i;
        qsToUpdate.add(sqToUpdate);
    }
    system.debug('[FormAndQuestionController]-updateOrderList : ' + qsToUpdate);
    update qsToUpdate;
    
    return null;
  } 
  
  //------------------------------------------------------------------------------//  
  /** When requested from the page - when the user clicks on 'Update Order' -
      this function will reorganize the list so that it is displayed in the new order
   */
   public Pagereference refreshQuestionList(){
    setupQuestionList();
    return null;
   }

//------------------------------------------------------------------------------//  
  
//------------------------------------------------------------------------------//    


//------------------------------------------------------------------------------//    
  

   /** Redirects the page that displays the detailed results of the form, 
       from all users who took the form.
    */
    public PageReference resultPage() {
      return new PageReference('/apex/ResultsPage?id='+formId);
    }

   
    
  
//------------------------------------------------------------------------------//      
  public Pagereference deleteRefresh(){
    system.debug('[FormAndQuestionController]-(deleteRefresh): go to delete with question id=' + questionReference);
    if (questionReference!= null )
    {
         Form_Question__c sq = [Select Id, Name FROM Form_Question__c WHERE Id =: questionReference];
         delete sq;  
         questionReference = null;  
    }
     allQuestions.clear();
     Double j = 0.0;
    List<Form_Question__c> allQuestionsObject = 
                    [Select Type__c, Id, Form__c, Required__c, 
                    Question__c, OrderNumber__c, Name, Choices__c
                    From Form_Question__c  
                    WHERE Form__c =: formId
                    order by OrderNumber__c];
    for (Integer i =0; i< allQuestionsObject.size(); i++){
      allQuestionsObject[i].OrderNumber__c= j;
      question theQ = new question(allQuestionsObject[i]);
      allQuestions.add(theQ);
      j = j+1.0;
    }
    responses = getResponses();
    try{
      update allQuestionsObject;
    }catch(Exception e){
      Apexpages.addMessages(e);
    }
      return saveOrUpdateReturn();
  }
//------------------------------------------------------------------------------//  

   /** 
    */
  public List<String> getResponses() {
    List<FormQuestionResponse__c> qr = [Select Form_Question__c, FormTaker__c, Response__c, Name From FormQuestionResponse__c limit 100];
    List<String> resp = new List<String>();
    for (FormQuestionResponse__c r : qr) {
      resp.add(r.Response__c);
    }
    
    return resp;
  }  

   /** Class: question
    *  Retrieves the question information and puts it in the question object
    */    
  public class question{
      public String   name                   {get; set;}
      public String   id                     {get; set;}
      public String   question               {get; set;}
      public String   orderNumber            {get; set;}
      public String   choices                {get; set;}
      public List<SelectOption> singleOptions{get; set;}
      public List<SelectOption> multiOptions {get; set;}
      public Boolean  required               {get; set;}
      public String   questionType           {get; set;}  
      public String   formName             {get; set;}
      public String   renderFreeText         {get; set;}
      //-- @CUONGPQ has add more variable to make define for text-field type. -- START 15 Apr 2016.
      public String   renderTextField         {get; set;}
      //-- END --.
      //-- @CUONGPQ has add more variable to make define for date-field type. -- START 26 Apr 2016.
      public String   renderDateField         {get; set;}
      //-- END --.
      //-- @CUONGPQ has add more variable to make define for password-field type. -- START 26 Apr 2016.
      public String   renderPasswordField         {get; set;}
      //-- END --.
      //-- @CUONGPQ has add more variable to make define for email-field type. -- START 27 Apr 2016.
      public String   renderEmailField         {get; set;}
      //-- END --.
      //-- @CUONGPQ has add more variable to make define for email-field type. -- START 27 Apr 2016.
      public String   renderTelField         {get; set;}
      //-- END --.
      public String   renderSelectRadio      {get; set;}
      public String   renderSelectCheckboxes {get; set;} 
      public String   renderSelectRow        {get; set;}
      public List<String> responses          {get; set;}
      public String   singleOptionsForChart  {get; set;}
      public String   qResultsForChart       {get; set;} 
      public List<String> strList            {get; set;} // The question's option as a list of string
      public List<Integer> resultsCounts     {get; set;} // The count of each response to a question's choices
      public List<SelectOption> rowOptions   {get; set;}
      public boolean  noData                 {get; set;}
      
    /** Fills up the question object
     *  param:  Form_Question__c 
       */     
     public question(Form_Question__c sq) {
        name = sq.Name;
        id = sq.Id;
        System.debug('in Question found one with id '+id);
        question = sq.Question__c;
        orderNumber = String.valueOf(sq.OrderNumber__c+1);
        choices = sq.Choices__c;
        required = sq.Required__c;
        questionType = sq.Type__c;
        singleOptionsForChart = ' ';
     if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_VERTICAL){
        //-- @CuongPQ has add more type textField. --START 15 Apr 2016.
        renderTextField='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='false';
        //-- END -- .
        
        renderSelectRadio='true';
        singleOptions = stringToSelectOptions(choices);
        renderSelectCheckboxes='false';
        renderFreeText='false';
        renderSelectRow = 'false';
      }
     else if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_MULTIPLE_SELECT_VERTICAL){
        //-- @CuongPQ has add more type textField. --START 15 Apr 2016.
        renderTextField='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='false';
        //-- END -- .  
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='false';
        //-- END -- .
          
        renderSelectCheckboxes='true';
        multiOptions = stringToSelectOptions(choices);
        renderSelectRadio='false';
        renderFreeText='false';
        renderSelectRow = 'false';
      }
     else if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_SINGLE_SELECT_HORIZONTAL){
        //-- @CuongPQ has add more type textField. --START 15 Apr 2016.
        renderTextField='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='false';
        //-- END -- .  
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='false';
        //-- END -- .
        
        renderSelectCheckboxes='false';
        rowOptions = stringToSelectOptions(choices);
        renderSelectRadio='false';
        renderFreeText='false';
        renderSelectRow = 'true';
      }
     else if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_FREE_TEXT){
        //-- @CuongPQ has add more type textField. --START 15 Apr 2016.
        renderTextField='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='false';
        //-- END -- .
        
        renderFreeText='true';
        renderSelectRadio='false';
        renderSelectCheckboxes='false';
        renderSelectRow = 'false';        
      }
      //--@CuongPQ has add more statement for check type 'text-field'. -- START 15 Apr 2016--.
      else if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_TEXT_FIELD){
        renderTextField='true';
        renderFreeText='false';
        renderSelectRadio='false';
        renderSelectCheckboxes='false';
        renderSelectRow = 'false';
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='false';
        //-- END -- .
      }
      //- END --.
      //--@CuongPQ has add more statement for check type 'date-field'. -- START 26 Apr 2016--.
      else if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_DATE_FIELD){
        renderTextField='false';
        renderFreeText='false';
        renderSelectRadio='false';
        renderSelectCheckboxes='false';
        renderSelectRow = 'false';
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='true';
        //-- END -- .
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='false';
        //-- END -- .
      }
      //--@CuongPQ has add more statement for check type 'password-field'. -- START 26 Apr 2016--.
      else if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_PASSWORD_FIELD){
        renderTextField='false';
        renderFreeText='false';
        renderSelectRadio='false';
        renderSelectCheckboxes='false';
        renderSelectRow = 'false';
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='true';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='false';
        //-- END -- .
      }
      //--@CuongPQ has add more statement for check type 'email-field'. -- START 27 Apr 2016--.
      else if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_EMAIL_FIELD){
        renderTextField='false';
        renderFreeText='false';
        renderSelectRadio='false';
        renderSelectCheckboxes='false';
        renderSelectRow = 'false';
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='true';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='false';
        //-- END -- .
      }
      //--@CuongPQ has add more statement for check type 'tel-field'. -- START 27 Apr 2016--.
      else if (sq.Type__c==System.Label.FORM_FORCE_QUESTION_TYPE_TEL_FIELD){
        renderTextField='false';
        renderFreeText='false';
        renderSelectRadio='false';
        renderSelectCheckboxes='false';
        renderSelectRow = 'false';
        
        //-- @CuongPQ has add more type dateField. --START 26 Apr 2016.
        renderDateField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type passwordField. --START 26 Apr 2016.
        renderPasswordField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type emailField. --START 27 Apr 2016.
        renderEmailField ='false';
        //-- END -- .
        
        //-- @CuongPQ has add more type telField. --START 27 Apr 2016.
        renderTelField ='true';
        //-- END -- .
      }
      //- END --.
        //responses= getResponses();
      }
      
     /** Splits up the string as given by the user and adds each option
      *  to a list to be displayed as option on the Visualforce page
      *  param: str String as submitted by the user
      *  returns the List of SelectOption for the visualforce page
        */  
      private List<SelectOption> stringToSelectOptions(String str){
      if (str == '')
      {
        return new List<SelectOption>();
      }
      strList = str.split('\n');
    
      List<SelectOption> returnVal = new List<SelectOption>();
      for(String s: strList){
        if (s!='') {
          returnVal.add(new SelectOption(s,s));
          if (s != 'null' && s!= null) {
            String sBis = s.replace(' ', '%20');
            singleOptionsForChart += s.trim()+'|';
          }
        }
      }
      singleOptionsForChart = singleOptionsForChart.substring(0, singleOptionsForChart.length()-1);
      return returnVal;
    } 
  }
  
  /** Fills up the List of questions to be displayed on the Visualforce page
   */ 
  public List<question> getAQuestion() {
    List<Form_Question__c> allQuestionsObject = 
                    [Select s.Type__c, s.Id, s.Form__c, s.Required__c, s.Question__c, 
                    s.OrderNumber__c, s.Name, s.Choices__c 
                    From Form_Question__c s 
                    WHERE s.Form__c =: formId ORDER BY s.OrderNumber__c ASC NULLS LAST];
    allQuestions = new List<question>();
    
    Double old_OrderNumber = 0;
    Double new_OrderNumber;
    Double difference = 0;
    /* Make sure that the order number follow each other (after deleting a question, orders might not do so) */
    for (Form_Question__c q : allQuestionsObject){ 
      new_OrderNumber = q.OrderNumber__c;
      difference = new_OrderNumber - old_OrderNumber - 1;
      if (difference > 0) {
        Double dd = double.valueOf(difference);
        Integer newOrderInt = dd.intValue();
        q.OrderNumber__c -= Integer.valueOf(newOrderInt); 
      }
      old_OrderNumber = q.OrderNumber__c;
      question theQ = new question(q);
      allQuestions.add(theQ);
    }
    allQuestionsSize = allQuestions.size();
    return allQuestions;
  } 

}