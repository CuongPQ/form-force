@isTest
public class FormhomeControllerTest
{
    public static TestMethod void initFormhomeController()
    {
        FormTestingUtil tu = new FormTestingUtil();
        FormhomeController homeCtr = new FormhomeController();
        system.assert(homeCtr.getFormListOptions().size() == 1);
        system.assert(homeCtr.contactId == null);
        system.assert(homeCtr.caseId == null);
        
        homeCtr.selectedForm = tu.formId;
        
        system.assert(homeCtr.getIsSelectForm() == true);
        
        system.assert(homeCtr.getPageName().contains('MainForm') == true);        
    }
}