global with sharing class GFormsController {
  
  //public String formDelete                       {get; set;}
  public String newFormName                      {get; set;}
  public List<miniForm>allForms                {get; set;}
  public String username                           {get; set;}
  //public String userId                             {get; set;}
  public String POD                                {get; set;}
  public List<Selectoption>  sitesPicklist         {get; set;} 
  public String formSite                         {get; set;}
  public String showAddSitesMessage                {get; set;} 
  private String subdomain;
  private boolean useTopLevelDomain;
  
  //@CuongPQ has define variable to store the form type. --START 20 Apr 2016.
  private String formType {get;set;}
  //--END--.
  
  public String siteInfo  {get; set;}
  
  public GFormsController (ApexPages.StandardController stdController){
    //@CuongPQ added to get the current recordType checked by user. --START 20 Apr 2016.
    try
    {
        Form__c recordForm = (Form__c)stdController.getRecord();
        this.formType = recordForm.RecordTypeId;
    }
    catch(Exception e)
    {
        System.debug('[GFormsController]-GFormsController constructor error: ' + e.getMessage());
    }
    //--END--.
    username = System.Userinfo.getUserId();    
    setAllForms();
    setupSitesPicklist();
    siteInfo = Site.getDomain();
  }
  
  
  private void setupSitesPicklist(){
    List<FormSitesUtil.FormSiteInfo> sites = new FormSitesUtil().getSiteList();
    if (sites.size() > 0) {
        subdomain = sites[0].Subdomain;
    }
    useTopLevelDomain=false;
    String pathPrefix;
    sitesPicklist = new List<Selectoption>();
//    List<Site> sites = [select Name, Subdomain, TopLevelDomain, UrlPathPrefix from Site where Status = 'Active' ];
    //setupDomain(sites);
    setupShowSites(sites.size());
    sitesPicklist.add(new Selectoption('--SELECT SITE--',System.Label.FORM_FORCE_SELECTSITE ));
    sitesPicklist.add(new Selectoption('Internal',System.Label.FORM_FORCE_Internal));
    for(FormSitesUtil.FormSiteInfo s : sites){
      if(s.prefix == null)
        pathPrefix='EMPTY';
      else
        pathPrefix=s.prefix;
      sitesPicklist.add(new Selectoption(pathPrefix, s.Name));
    }
  }

/*
  private void setupDomain(list <Site> sites){
    String subD;
    String tLevelDomain;
    useTopLevelDomain =false;
    if(sites!=null && sites.size()>0){
      if(sites[0].TopLevelDomain != null && sites[0].TopLevelDomain.length()>0){
        subdomain = sites[0].TopLevelDomain;
        useTopLevelDomain=true;
      }
      else
        subdomain = sites[0].Subdomain;
    }
  }
*/
  private void setupShowSites(Integer i){
    if(i>0){
      showAddSitesMessage = 'false';
    }
    else{
      showAddSitesMessage = 'true';
    }
  }

  private void setAllForms(){
  
  //@CuongPQ has add check by Admin to make decision show form result by record type. START 19 Apr 2016.
  String currentRecordTypeId;
  if (false == FormUtils.getModifyAllPermission())
  {
      List<RecordType> listRecordType = [SELECT Id FROM RecordType WHERE Name=:System.Label.FORM_FORCE_FORM_RECORD_TYPE_MASTER];
      currentRecordTypeId = listRecordType.size() > 0 ? listRecordType[0].Id : null;
  }
  else
  {
      currentRecordTypeId = ApexPages.currentPage().getParameters().get(FormUtils.RECORD_TYPE_PARAMETER);
  }
  
  //@CuongPQ change the selector.
   //List<Form__c> forms = [Select Id, Name from Form__c where IsDeleted = false order by Name];
   List<Form__c> forms = getFormByRecordTypeId(currentRecordTypeId);
   //END 19 Apr 2016.
  
   if(allForms == null){
      allForms = new List<miniForm>();
    }
   else{
      allForms.clear(); 
    }
   for(Form__c s : forms){
      allForms.add(new miniForm(s.Name, s.Id));
    }
  }
  
   /**
    * --getFormByRecordTypeId--
    * @Author: CuongPQ.
    * @Date: 19 Apr 2016.
    * @Description: get list form by record type id.
    * @Param: - pId: id of recordType.
    * @return: List<Form__c> list form data.
    */
    public List<Form__c> getFormByRecordTypeId(String pId)
    {
        if (pId instanceOf Id)
        {
            return [Select Id, Name from Form__c where IsDeleted = false AND RecordTypeId=:pId order by Name];
        }
        else
        {
            return [Select Id, Name from Form__c where IsDeleted = false order by Name];
        }
    }

  public Pagereference makeNewForm(){   
    if(newFormName==null){
     POD='';
     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.FORM_FORCE_Form_Name_Is_Required));
     return null;
    }
    if(formSite == '--SELECT SITE--'){
     POD='';
     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.FORM_FORCE_Form_Site_Is_Required));
     return null;
    }
    
    
    
    String urlPrefix = setupUrlPrefix(formSite);
    String domain = setupDomainForForm(POD);
    String urlToSave= domain+'/'+urlPrefix+'MainForm?';
    if (formSite == 'Internal')
    {
        urlToSave = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/' + 'MainForm?';
    }
    
    Form__c s = new Form__c();
    s.Name = newFormName;
    //@CuongPQ has add init field FormMain_Header__c follow designed.--START--29 Apr 2016.
    s.FormMain_Header__c = newFormName;
    //--END--.
    s.URL__c = urlToSave;
    s.Submit_Response__c = 'empty';
    
    //@CuongPQ has commentted 2 line code below because it do not use.--START 09 May 2016.
    //s.thankYouText__c = System.Label.FORM_FORCE_Form_Submitted_Thank_you;
    //s.thankYouLink__c='http://www.salesforce.com';
    
    //@CuongPQ has added init for thank form text.
    s.FormThanks_NameText__c = newFormName + System.Label.FORM_FORCE_THANKFORM_NUMBER;
    
    //@CuongPQ has added recordty id into here. -- START 20 Apr 2016.
    if (this.formType instanceOf Id)
    {
        s.RecordTypeId = this.formType;
    }
    //--END--.
    try{
     insert s;
     createAnonFormTaker(s.Id);
     return new Pagereference('/apex/FormManagerPage?id='+s.Id);
    }catch(Exception e){
      return null;
    }
  }

  private static void createAnonFormTaker(String formId){
     list <FormTaker__c> sTakers = [select id from FormTaker__c where Form__c = :formId and Contact__c = null and Case__c = null];
     if(sTakers != null && sTakers.size()>0)
       return;
     FormTaker__c st = new FormTaker__c();      
     st.Case__c = null;
     st.Form__c = formId;
     st.Contact__c = null;
     insert st;
  }
  
  private String setupDomainForForm(String pod){
    if(pod != 'NO_POD' && !useTopLevelDomain && checkSubdomain(subdomain)){
      return 'http://'+subdomain+'.'+pod+'.force.com';
    }
    else if(pod != 'NO_POD' && useTopLevelDomain && checkSubdomain(subdomain)){
      return 'http://'+subdomain+'.'+pod;
    }
    else if(useTopLevelDomain) {
      return 'http://'+subdomain;   
    }
    else{
      return 'http://'+subdomain+'.force.com';
    }
  }
  
  private boolean checkSubdomain(String subdomain){
    if(subdomain == null)
     return false;
    else if (subdomain.contains('developer-edition'))
     return true;
    else
     return false;
  }
  
  private String setupUrlPrefix(String site){
    if(site == null || site=='EMPTY')
     return '';
    else
     return site+'/';
  }

  public Pagereference doFormReset(){
    setAllForms();
    return null;
  }
  
  public static webservice String deleteForm(String deleteId){
    Form__c s= [Select Id, Name from Form__c where Id =:deleteId];
    delete s;
    
    return 'true';
  }

  public Class miniForm{
    public String sName {get; set;}
    public String sId   {get; set;}
    
    public miniForm(String miniName, String miniId){
      sName = miniName;
      sId = miniId;
    } 
      
  }  
    
}