@isTest
public class SObjectLookupControllerTest{
    
    private static Testmethod void testLookupContact(){
        
        Apexpages.currentPage().getParameters().put('key_prefix','003');
        Apexpages.currentPage().getParameters().put('txt','j_id0:j_id1:theForm:contact_field:j_id17:inputObj');
        Apexpages.currentPage().getParameters().put('frm','j_id0:j_id1:theForm');
        SObjectLookupController cqc = new SObjectLookupController();
        system.assert(cqc.objName == 'Contact');
        //system.assert(cqc.getIsExistedFieldsetNew() == false);
        //system.assert(cqc.getIsExistedFieldsetSearchCondition() == false);
        //system.assert(cqc.getIsExistedFieldsetSearchResults() == false);
        system.assert(cqc.getTextBox() == 'j_id0:j_id1:theForm:contact_field:j_id17:inputObj');
        system.assert(cqc.getFormTag() == 'j_id0:j_id1:theForm');
        
        List<String> listFieldsSet = cqc.getListFieldSet();
        system.assert(listFieldsSet.size() >=0);
        
        cqc.search();
        system.assert(cqc.results.size() >=0);
        
        Contact c = new Contact();
        c.LastName = 'Doe';
        c.FirstName = 'John';
        c.Email = 'formAppUser@hotmail.com';
        c.Password__c ='abc123456';
        c.mainOwner__c = 'test common user';
        cqc.sobj = c;
        cqc.saveOjbRecord();
        system.assert(cqc.sobj.Id != c.Id); 
        
    }
    
    private static Testmethod void testLookupCase(){
        
        Apexpages.currentPage().getParameters().put('key_prefix','500');
        SObjectLookupController cqc = new SObjectLookupController();
        system.assert(cqc.objName == 'Case');
        system.assert(cqc.getIsExistedFieldsetNew() == false);
        system.assert(cqc.getIsExistedFieldsetSearchCondition() == false);
        system.assert(cqc.getIsExistedFieldsetSearchResults() == false);
        
        List<String> listFieldsSet = cqc.getListFieldSet();
        system.assert(listFieldsSet.size() >=0);
        
        system.assert(cqc.getPermissionCreate() == true);
        cqc.searchString = 'demo';
        cqc.search();
        system.assert(cqc.results.size() >=0);
    }
    
    private static Testmethod void testLookupUnknow(){
        
        Apexpages.currentPage().getParameters().put('key_prefix','abc');
        SObjectLookupController cqc = new SObjectLookupController();
        system.assert(cqc.objName == null);
        system.assert(cqc.getIsExistedFieldsetNew() == false);
        system.assert(cqc.getIsExistedFieldsetSearchCondition() == false);
        system.assert(cqc.getIsExistedFieldsetSearchResults() == false);
        system.assert(cqc.getPermissionCreate() == false);
    }
}