trigger ContactEncryptPasswordTrigger on Contact (Before Insert, Before update) {

    //loop all new data and create encrypt password.
    for (Contact item : Trigger.New)
    {
        if (item.Password__c == null || item.Password__c.length() == 0){
        }else{
            item.Encrypt_Password__c = InquerySiteUserCommonInfo.encryptData(item.Password__c ,InquerySiteUserCommonInfo.PASSWORD_CRYPTO_KEY);
        }
    }
}